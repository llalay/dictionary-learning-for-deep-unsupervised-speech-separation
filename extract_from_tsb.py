import pandas as pd, matplotlib.pyplot as plt, numpy as np
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
from pathlib import Path


def plot_settings():
    # Run this at the beginning

    # Bigger title font
    plt.rcParams["axes.titlesize"] = 30

    # Bigger label font
    plt.rcParams["axes.labelsize"] = 20

    # Bigger legend
    plt.rcParams["legend.fontsize"] = 20

    # Larger line width
    plt.rcParams["lines.linewidth"] = 2.5

    # Change default figsize
    plt.rcParams["figure.figsize"] = (12, 8)


class TensorboardExtractor:
    val_dict: dict
    train_dict: dict

    def __init__(self, path, name=None) -> None:
        self.name = name
        self.path = Path(path)
        self.event_acc = EventAccumulator(str(self.path))
        self.event_acc.Reload()
        self.val_scalars = [
            "Audio reconstruction/si_sdr",
            "Audio reconstruction/si_snr",
            "Sparsity/hoyer",
            "loss/val_loss",
        ]
        self.train_scalars = [
            "loss/expectation_loss",
            "loss/kl_loss",
            "loss/train_loss",
        ]
        self.val_dict = None
        self.train_dict = None

        for s in self.val_scalars + self.train_scalars:
            assert (
                s in self.event_acc.Tags()["scalars"]
            ), f"Scalar {s} not found in tensorboard file."

    def get_val_scalars(self):
        # Dict to store each scalar's important data
        self.val_dict = dict()
        for s in self.val_scalars:
            scalar_dict = dict()
            data = self.event_acc.Scalars(s)
            scalar_dict["data_frame"] = pd.DataFrame(data)
            scalar_dict["steps"] = list(set(scalar_dict["data_frame"]["step"]))
            scalar_dict["steps"].sort()

            # As all validation computations are done on the same step we just
            # take the average
            average = list()
            for st in scalar_dict["steps"]:
                mean = np.mean(scalar_dict["data_frame"].query(f"step=={st}").value)
                average.append(mean)
            scalar_dict["average"] = average
            self.val_dict[s] = scalar_dict

    def get_train_scalars(self):
        self.train_dict = dict()
        for s in self.train_scalars:
            data = self.event_acc.Scalars(s)
            df = pd.DataFrame(data)
            smoothed_values = df.ewm(alpha=0.05).mean()
            df["smoothed_values"] = smoothed_values["value"]
            self.train_dict[s] = df

    def get_val_plot_data(self, scalar):
        if self.val_dict is None:
            self.get_val_scalars()

        output = dict()

        output["title"] = scalar.split("/")[-1].replace("_", " ").title()
        output["raw_idx"] = self.val_dict[scalar]["data_frame"]["step"]
        output["raw_data"] = self.val_dict[scalar]["data_frame"]["value"]
        output["average"] = self.val_dict[scalar]["average"]
        output["average_idx"] = self.val_dict[scalar]["steps"]

        return output

    def get_train_plot_data(self, scalar):
        if self.train_dict is None:
            self.get_train_scalars()
        output = dict()

        output["title"] = scalar.split("/")[-1].replace("_", " ").title()
        output["raw_idx"] = self.train_dict[scalar]["step"]
        output["raw_data"] = self.train_dict[scalar]["value"]
        output["smoothed_data"] = self.train_dict[scalar]["smoothed_values"]
        output["smoothed_idx"] = self.train_dict[scalar]["step"]

        return output

    def save_plot(self, scalar):
        plot_settings()
        save_dir = self.path / "plots"
        val_save_dir = save_dir / "val"
        val_save_dir.mkdir(exist_ok=True, parents=True)
        train_save_dir = save_dir / "train"
        train_save_dir.mkdir(exist_ok=True, parents=True)

        if scalar in self.val_scalars:
            plot_info = self.get_val_plot_data(scalar)
            fig = plt.figure()
            ax = fig.gca()
            ax.plot(plot_info["raw_idx"], plot_info["raw_data"], ".", label="Raw")
            ax.plot(plot_info["average_idx"], plot_info["average"], label="Average")
            ax.set_xlabel("Steps")
            ax.set_ylabel("Value")
            ax.set_title(plot_info["title"])
            ax.legend()
            fig.savefig(val_save_dir / f"{plot_info['title']}.pdf")
        elif scalar in self.train_scalars:
            plot_info = self.get_train_plot_data(scalar)
            fig = plt.figure()
            ax = fig.gca()
            ax.plot(plot_info["raw_idx"], plot_info["raw_data"], ".", label="Raw")
            ax.plot(
                plot_info["smoothed_idx"],
                plot_info["smoothed_data"],
                label="Smoothed",
            )
            ax.set_xlabel("Steps")
            ax.set_ylabel("Value")
            ax.set_title(plot_info["title"])
            ax.legend()
            fig.savefig(train_save_dir / f"{plot_info['title']}.pdf")
        else:
            raise ValueError(f"Scalar {scalar} not found.")


def compare_scalars(scalar, tbes: list[TensorboardExtractor]):
    plot_settings()
    validation = False
    if scalar in tbes[0].val_scalars:
        validation = True

    fig = plt.figure()
    ax = fig.gca()
    if validation:
        plot_infos = [tbe.get_val_plot_data(scalar) for tbe in tbes]
        for k, info in enumerate(plot_infos):
            ax.plot(info["average_idx"], info["average"], label=tbes[k].name)
        ax.set_xlabel("Steps")
        ax.set_ylabel("Value")
        ax.set_title(info["title"])
        ax.legend()
    else:
        plot_infos = [tbe.get_train_plot_data(scalar) for tbe in tbes]
        for k, info in enumerate(plot_infos):
            ax.plot(info["smoothed_idx"], info["smoothed_data"], label=tbes[k].name)
        ax.set_xlabel("Steps")
        ax.set_ylabel("Value")
        ax.set_title(info["title"])
        ax.legend()
        # TODO: Fix legends that do not show up
    return fig


def main():
    plot_settings()
    logdir = Path("/home/louis/Documents/Logs/Latent_64_Dict_64_Hidden_128")

    print("Start init")

    tbe1 = TensorboardExtractor(logdir / "LibVaeEp120")
    tbe2 = TensorboardExtractor(logdir / "LibSdmEp120")
    tbe3 = TensorboardExtractor(logdir / "TcdSdmEp300")
    tbe4 = TensorboardExtractor(logdir / "TcdVaeEp150")

    print("Init done")

    vae_fig = compare_scalars("Audio reconstruction/si_sdr", [tbe1, tbe4])
    sdm_fig = compare_scalars("Audio reconstruction/si_sdr", [tbe2, tbe3])

    vae_fig.show()
    sdm_fig.show()
    _ = input("Press enter to exit")


if __name__ == "__main__":
    main()
