from . import (
    sdm_vae_speaker_specific_dict,
    utils,
    sdm_vae,
    vae,
    sdm_ae,
    ae,
    sdm_vae_multi_dict,
    sdm_supervised_dict_specific,
    sdm_pretrained,
)
from typing import TypedDict


class SupportedModels(TypedDict):
    sdm_vae: sdm_vae.SDM_VAE
    vae: vae.VAE
    sdm_ae: sdm_ae.SDM_AE
    ae: ae.AE
    sdm_vae_multi_dict: sdm_vae_multi_dict.SDM_VAEMultiDict
    sdm_vae_dict_learning: sdm_vae_speaker_specific_dict.SDM_VAESpeakerSpecificDict
    sdm_supervised_dict_specific: sdm_supervised_dict_specific.SupervisedDictSpecific


supported_models = SupportedModels(
    sdm_vae=sdm_vae.SDM_VAE,
    vae=vae.VAE,
    sdm_ae=sdm_ae.SDM_AE,
    ae=ae.AE,
    sdm_vae_multi_dict=sdm_vae_multi_dict.SDM_VAEMultiDict,
    sdm_vae_dict_learning=sdm_vae_speaker_specific_dict.SDM_VAESpeakerSpecificDict,
    sdm_supervised_dict_specific=sdm_supervised_dict_specific.SupervisedDictSpecific,
)
