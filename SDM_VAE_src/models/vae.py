import copy, torch, torch.nn as nn
from typing import Any
from pytorch_lightning import Callback, LightningModule, Trainer
from pytorch_lightning.utilities.types import STEP_OUTPUT
from SDM_VAE_src.dataprocessing.transforms import SingleAudioSpecTransform
from SDM_VAE_src.models import utils
from SDM_VAE_src.metrics import supported_audio_metrics, supported_sparse_metrics


class VAE(LightningModule):
    # All methods marked as 'built in' are automatically called by pytorch lightning
    def __init__(
        self,
        config: dict = None,
        mini_batch: dict = None,
        transform: SingleAudioSpecTransform = None,
    ) -> None:
        super().__init__()
        self.save_hyperparameters(ignore=["transform"])

        self.transform = copy.deepcopy(transform)

        # Different Layers of the VAE
        self.config = config
        config_encoder, config_decoder = self.init_encoder_decoder()
        self.encoder = utils.Encoder(**config_encoder)
        self.decoder = utils.Decoder(**config_decoder)
        self.log_var = nn.Linear(
            self.encoder.encode_dim, config["network"]["latent_dim"]
        )
        self.mean = nn.Linear(self.encoder.encode_dim, config["network"]["latent_dim"])

        self.input_dim = config["network"]["input_dim"]
        self.lr = config["optimizer"]["learning_rate"]

        # Losses
        self.expectation_loss = utils.ExpectationLoss()
        self.time_domain_loss = utils.L1Loss()
        self.kl_loss = utils.GaussianKLDivLoss()
        self.beta = config["training"]["beta"]
        self.repametrization = utils.GaussianReparametrization()

        # Callbacks
        self.init_callbacks(mini_batch)

    def init_encoder_decoder(self):
        config_n = self.config["network"]
        match config_n["activation"].lower():
            case "relu":
                activation = nn.ReLU()
            case "tanh":
                activation = nn.Tanh()
            case _:
                print("Use ReLU by default")
                activation = nn.ReLU()
        match config_n["mode"]:
            case "linear":
                config_encoder = {
                    "input_dim": config_n["input_dim"],
                    "hidden_dims": config_n["linear"]["hidden_dims"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
                config_decoder = {
                    "latent_dim": config_n["latent_dim"],
                    "hidden_dims": list(reversed(config_n["linear"]["hidden_dims"])),
                    "output_dim": config_n["input_dim"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
            case _:
                print("Wrong mode, try linear instead")
        return config_encoder, config_decoder

    def init_callbacks(self, mini_batch: dict):
        callbacks = []
        # Audio metrics
        callbacks.append(AudioMetrics(self.config["metrics"], mini_batch))
        # Sparse metrics
        callbacks.append(SparseMetrics(self.config["metrics"], mini_batch))
        # Audio reconstruction
        callbacks.append(ReconstructedAudioLogger(self.config["global sr"], mini_batch))

        self.callbacks = callbacks

    def get_input(self, batch):
        """Modify this function according to the collate function of the dataloader"""
        return batch["input"]

    def forward(self, x) -> dict:
        # Built in

        # Variational encoding
        encoded_vector = self.encoder(x)
        mean = self.mean(encoded_vector)
        log_var = self.log_var(encoded_vector)

        # Update Prior distribution
        with torch.no_grad():
            var_prior = torch.exp(log_var) + torch.square(mean)

        # Reparametrization trick
        latent_vector = self.repametrization(mean, log_var)

        # Reconstruction
        predictions = torch.exp(self.decoder(latent_vector))

        return {
            "predictions": predictions,
            "activations": latent_vector,
            "current_distribution": (mean, log_var),
            "prior_distribution": (torch.zeros_like(mean), var_prior),
        }

    def configure_optimizers(self):
        # Built in
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def loss(self, batch, outputs):
        # Check the loss type from the config file
        config_m = self.config["model"]
        if "time" in config_m["loss_domain"]:
            # Extract audios
            reconstructions = self.reconstruct(batch, outputs)
            in_audios = batch["audios"]

            # Compute loss
            reconstruction_loss = self.time_domain_loss(
                reconstructions,
                in_audios,
            )
            loss = reconstruction_loss
        else:
            expectation_loss = self.expectation_loss(
                predictions=outputs["predictions"],
                target=self.get_input(batch),
            )
            loss = expectation_loss

        KL_loss = self.kl_loss(
            mean_prior=outputs["prior_distribution"][0],
            var_prior=outputs["prior_distribution"][1],
            mean_current=outputs["current_distribution"][0],
            var_current=torch.exp(outputs["current_distribution"][1]),
        )

        # Log losses during training
        if self.training:
            self.log("loss/expectation_loss", loss, batch_size=self.batch_size)
            self.log("loss/kl_loss", KL_loss, batch_size=self.batch_size)

        elbo = loss + self.beta * KL_loss

        return elbo

    def training_step(self, batch, batch_idx):
        # Built in

        samples = self.get_input(batch)
        outputs = self(samples)
        self.batch_size = samples.shape[0]

        loss = self.loss(batch, outputs)
        self.log("loss/train_loss", loss, batch_size=self.batch_size)

        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        # Built in. Model is automatically set to eval and no grad mode

        samples = self.get_input(batch)
        outputs = self(samples)
        self.batch_size = samples.shape[0]

        loss = self.loss(batch, outputs)
        self.log("loss/val_loss", loss, batch_size=self.batch_size)

        outputs["loss"] = loss

        return outputs

    def test_step(self, batch, batch_idx):
        samples = self.get_input(batch)
        outputs = self(samples)
        outputs["ground_truth"] = samples

        return outputs

    def reconstruct(self, batch: dict, outputs: dict):
        # Unpack arguments
        predictions_frames = outputs["predictions"]
        num_frames = batch["num_frames"]
        input_phases = batch["phases"]

        output_specs = self.transform.unstack(predictions_frames, num_frames)

        # Reconstruct audio
        output_audios = [
            self.transform.inverse(spec, phase.to(spec))
            for spec, phase in zip(output_specs, input_phases)
        ]

        for a in output_audios:
            a = a / torch.max(torch.abs(a))

        return output_audios

    def debug_print(self):
        for component in self.children():
            print(component)
        print(f"Dictionnary shape: {self.dictionary.shape}")


class ReconstructedAudioLogger(Callback):
    def __init__(self, sr: int, mini_batch: dict) -> None:
        super().__init__()
        self.sr = sr
        self.batch = mini_batch

    def log_audio(
        self,
        audio: torch.Tensor,
        name: str,
        trainer: Trainer,
    ) -> None:
        trainer.logger.experiment.add_audio(
            name,
            audio,
            trainer.global_step,
            sample_rate=self.sr,
        )

    def on_train_start(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        for i, audio in enumerate(self.batch["audios"]):
            self.log_audio(audio, f"Input/{i}", trainer)

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: VAE,
    ) -> None:
        samples = pl_module.get_input(self.batch)

        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)

        output_audios = pl_module.reconstruct(self.batch, pl_module(samples))
        for i, audio in enumerate(output_audios):
            self.log_audio(audio, f"Reconstruction/{i}", trainer)


class AudioMetrics(Callback):
    def __init__(self, config_m: dict, mini_batch: dict) -> None:
        super().__init__()
        self.batch = mini_batch
        metrics_list = config_m["audio_metrics"]
        self.metrics = {}
        for m in metrics_list:
            assert m in supported_audio_metrics, f"{m} is not supported."
            assert (
                m in config_m
            ), f"{m} is not in the config file. Add the appropriate arguments."
            init_args = config_m[m]
            instance = supported_audio_metrics[m](**init_args)
            self.metrics[m] = instance

    def compute_scores(self, ground_truths, predictions):
        scores = {}
        for m in self.metrics:
            value = 0
            for i in range(len(ground_truths)):
                value += self.metrics[m](
                    target=ground_truths[i].to(predictions[i]), preds=predictions[i]
                )
            scores[m] = value / len(ground_truths)
        return scores

    def log_scores(self, scores: dict, name: str, trainer: Trainer):
        for m in scores:
            trainer.logger.experiment.add_scalar(
                f"{name}/{m}",
                scores[m],
                trainer.global_step,
            )

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: VAE,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        samples = pl_module.get_input(self.batch)
        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)
        in_audios = self.batch["audios"]
        predictions_audios = pl_module.reconstruct(self.batch, pl_module(samples))
        scores = self.compute_scores(in_audios, predictions_audios)
        self.log_scores(scores, "Audio reconstruction", trainer)

    def on_test_batch_end(
        self,
        trainer: Trainer,
        pl_module: VAE,
        outputs: STEP_OUTPUT | None,
        batch: Any,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        in_audios = batch["audios"]
        predictions_audios = pl_module.reconstruct(batch, outputs)
        scores = self.compute_scores(in_audios, predictions_audios)
        self.log_scores(scores, "Test", trainer)


class SparseMetrics(Callback):
    def __init__(self, config_m: dict, mini_batch: dict) -> None:
        super().__init__()
        self.batch = mini_batch
        metrics_list = config_m["sparse_metrics"]
        self.metrics = {}
        for m in metrics_list:
            assert m in supported_sparse_metrics, f"{m} is not supported."
            assert (
                m in config_m
            ), f"{m} is not in the config file. Add the appropriate arguments."
            init_args = config_m[m]
            instance = supported_sparse_metrics[m](**init_args)
            self.metrics[m] = instance

    def compute_scores(self, vectors: torch.Tensor):
        scores = {}
        for m in self.metrics:
            scores[m] = self.metrics[m](vectors)
        return scores

    def log_scores(self, scores: dict, name: str, trainer: Trainer):
        for m in scores:
            trainer.logger.experiment.add_scalar(
                f"{name}/{m}",
                scores[m],
                trainer.global_step,
            )

    def on_validation_epoch_end(self, trainer: Trainer, pl_module: VAE) -> None:
        samples = pl_module.get_input(self.batch)
        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        outputs = pl_module(samples)
        vectors = outputs["activations"]
        scores = self.compute_scores(vectors)
        self.log_scores(scores, "Sparsity", trainer)

    def on_test_batch_end(
        self,
        trainer: Trainer,
        pl_module: VAE,
        outputs: STEP_OUTPUT | None,
        batch: Any,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        vectors = outputs["activations"]
        scores = self.compute_scores(vectors)
        self.log_scores(scores, "Test", trainer)
