import torch.nn as nn, torch, copy
from typing import Any, Optional
from pytorch_lightning import Callback, LightningModule, Trainer
from pytorch_lightning.utilities.types import STEP_OUTPUT
from SDM_VAE_src.dataprocessing.transforms import SourceSeparationSpecTransform
from SDM_VAE_src.models import utils
from SDM_VAE_src.metrics import supported_audio_metrics, supported_sparse_metrics


class SDM_VAESpeakerSpecificDict(LightningModule):
    # All methods marked as 'built in' are automatically called by pytorch lightning
    def __init__(
        self,
        config: dict = None,
        mini_batch: dict = None,
        transform: SourceSeparationSpecTransform = None,
    ) -> None:
        super().__init__()
        self.save_hyperparameters(ignore="transform")

        self.transform = copy.deepcopy(transform)

        # Different Layers of the VAE
        self.config = config
        config_encoder, config_decoder = self.init_encoder_decoder()
        self.encoder = utils.Encoder(**config_encoder)
        self.decoder = utils.Decoder(**config_decoder)
        self.log_var = nn.Linear(
            self.encoder.encode_dim,
            config["network"]["dict_dim"],
        )
        self.mean = nn.Linear(
            self.encoder.encode_dim,
            config["network"]["dict_dim"],
        )

        self.input_dim = config["network"]["input_dim"]
        self.lr = config["optimizer"]["learning_rate"]

        self.num_speaker = len(mini_batch["all_speakers"])
        self.speaker2idx = {
            speaker: idx for idx, speaker in enumerate(mini_batch["all_speakers"])
        }
        dictionaries = torch.rand(
            self.num_speaker,
            config["network"]["latent_dim"],
            config["network"]["dict_dim"],
        )
        if not config["network"]["random_dict"]:
            for k in range(self.num_speaker):
                dictionaries[k] = utils.create_dictionary_dct(
                    config["network"]["latent_dim"],
                    config["network"]["dict_dim"],
                )
        self.dictionary = nn.Parameter(dictionaries, requires_grad=True)

        # Losses
        self.expectation_loss = utils.ExpectationLoss()
        self.kl_loss = utils.GaussianKLDivLoss()
        self.time_domain_loss = utils.L1Loss()
        self.beta = config["training"]["beta"]
        self.repametrization = utils.GaussianReparametrization()

        self.init_callbacks(mini_batch)

    def init_encoder_decoder(self):
        config_n = self.config["network"]
        match config_n["activation"].lower():
            case "relu":
                activation = nn.ReLU()
            case "tanh":
                activation = nn.Tanh()
            case _:
                print("Use ReLU by default")
                activation = nn.ReLU()
        match config_n["mode"]:
            case "linear":
                config_encoder = {
                    "input_dim": config_n["input_dim"],
                    "hidden_dims": config_n["linear"]["hidden_dims"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
                config_decoder = {
                    "latent_dim": config_n["latent_dim"],
                    "hidden_dims": list(reversed(config_n["linear"]["hidden_dims"])),
                    "output_dim": config_n["input_dim"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
            case _:
                print("Wrong mode, try linear instead")
        return config_encoder, config_decoder

    def init_callbacks(self, mini_batch: dict = None):
        # Initiliaze model specific callbacks
        self.callbacks = []

        log_audio = ReconstructedAudioLogger(self.config["global sr"], mini_batch)
        self.callbacks.append(log_audio)

        log_dict = DictionaryLogger()
        self.callbacks.append(log_dict)

        log_sparse = SparseMetrics(self.config["metrics"], mini_batch)
        self.callbacks.append(log_sparse)

        log_audio_metrics = AudioMetrics(self.config["metrics"], mini_batch)
        self.callbacks.append(log_audio_metrics)

    def get_input(self, batch):
        """Modify this function according to the collate function of the dataloader"""
        return batch["input"]

    def forward(self, x, speakers, num_frames) -> dict:
        # Built in

        # Variational encoding
        encoded_vector = self.encoder(x)
        mean = self.mean(encoded_vector)
        log_var = self.log_var(encoded_vector)

        # Update Prior distribution
        with torch.no_grad():
            var_prior = torch.exp(log_var) + torch.square(mean)

        # Reparametrization trick
        activation_vector: torch.Tensor = self.repametrization(mean, log_var)

        # Compute the latent vectors for each speaker
        latent_vector = activation_vector[: num_frames[0]] @ torch.transpose(
            self.dictionary[self.speaker2idx[speakers[0]]],
            0,
            1,
        )
        for i, s in enumerate(speakers[1:]):
            z = activation_vector[
                num_frames[i] : num_frames[i] + num_frames[i + 1]
            ] @ torch.transpose(
                self.dictionary[self.speaker2idx[s]],
                0,
                1,
            )
            latent_vector = torch.cat((latent_vector, z), dim=0)

        reconstruction = torch.exp(self.decoder(latent_vector))

        return {
            "predictions": reconstruction,
            "sparse_vector": activation_vector,
            "current_distribution": (mean, log_var),
            "prior_distribution": (torch.zeros_like(mean), var_prior),
        }

    def forward_wrong_dict(self, x, speakers, num_frames) -> dict:
        # Variational encoding
        encoded_vector = self.encoder(x)
        mean = self.mean(encoded_vector)
        log_var = self.log_var(encoded_vector)

        # Reparametrization trick
        activation_vector: torch.Tensor = self.repametrization(mean, log_var)

        # Compute the latent vectors for each speaker
        good_index = self.speaker2idx[speakers[0]]
        wrong_index = (good_index + 1) % self.num_speaker
        latent_vector = activation_vector[: num_frames[0]] @ torch.transpose(
            self.dictionary[wrong_index],
            0,
            1,
        )
        for i, s in enumerate(speakers[1:]):
            good_index = self.speaker2idx[s]
            wrong_index = (good_index + 1) % self.num_speaker
            z = activation_vector[
                num_frames[i] : num_frames[i] + num_frames[i + 1]
            ] @ torch.transpose(
                self.dictionary[wrong_index],
                0,
                1,
            )
            latent_vector = torch.cat((latent_vector, z), dim=0)

        reconstruction = torch.exp(self.decoder(latent_vector))

        return {"predictions_wrong_dict": reconstruction}

    def configure_optimizers(self):
        # Built in
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def loss(self, batch, outputs):
        # Check the loss type from the config file
        config_m = self.config["model"]
        if "time" in config_m["loss_domain"]:
            # Extract audios
            reconstructions = self.reconstruct(batch, outputs)
            in_audios = batch["audios"]

            # Compute loss
            reconstruction_loss = self.time_domain_loss(
                reconstructions,
                in_audios,
            )
            loss = reconstruction_loss
        else:
            expectation_loss = self.expectation_loss(
                predictions=outputs["predictions"],
                target=self.get_input(batch),
            )
            loss = expectation_loss

        KL_loss = self.kl_loss(
            mean_prior=outputs["prior_distribution"][0],
            var_prior=outputs["prior_distribution"][1],
            mean_current=outputs["current_distribution"][0],
            var_current=torch.exp(outputs["current_distribution"][1]),
        )

        # Log losses during training
        if self.training:
            self.log("loss/expectation_loss", loss, batch_size=self.batch_size)
            self.log("loss/kl_loss", KL_loss, batch_size=self.batch_size)

        return loss + self.beta * KL_loss

    def training_step(self, batch, batch_idx):
        # Built in

        samples = self.get_input(batch)
        speakers = batch["speakers"]
        num_frames = batch["num_frames"]
        outputs = self(samples, speakers, num_frames)
        self.batch_size = samples.shape[0]

        loss = self.loss(batch, outputs)
        self.log("loss/train_loss", loss, batch_size=self.batch_size)

        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        # Built in. Model is automatically set to eval and no grad mode

        # No validation in supervised context
        pass

    def test_step(self, batch, batch_idx):
        speakers = batch["speakers"]
        sources = batch["audios"]

        # Create the mixture
        ## First pad the sources
        max_len = max([s.shape[0] for s in sources])
        sources = [
            torch.nn.functional.pad(s, (0, max_len - s.shape[0])) for s in sources
        ]
        mixture = torch.sum(torch.stack(sources), dim=0)

        # Compute the mixture spectrogram
        spec_dict_out = self.transform.spec(mixture, keep_phase=True)
        spec_mix = torch.transpose(spec_dict_out["spec"], 0, 1)

        new_batch = {
            "phases": [spec_dict_out["phase"]] * len(speakers),
            "num_frames": [spec_mix.shape[0]] * len(speakers),
        }

        # Forward with speaker defined dict
        ## Variational encoding
        encoded_vector = self.encoder(spec_mix)
        mean = self.mean(encoded_vector)
        log_var = self.log_var(encoded_vector)

        ## Reparametrization trick
        activation_vector: torch.Tensor = self.repametrization(mean, log_var)

        ## Compute the latent vectors for each speaker
        batch_size = len(activation_vector)
        latent_vector = torch.zeros(
            batch_size * len(speakers),
            self.config["network"]["latent_dim"],
            device=self.device,
        )
        for i, s in enumerate(speakers):
            start = i * batch_size
            stop = (i + 1) * batch_size
            latent_vector[start:stop] = activation_vector @ torch.transpose(
                self.dictionary[self.speaker2idx[s]],
                0,
                1,
            )
        predictions_frames = torch.exp(self.decoder(latent_vector))

        # Reconstruction
        reconstructed_sources = self.reconstruct(
            new_batch,
            {"predictions": predictions_frames},
        )
        # Log the audio of the 4 first batch
        if batch_idx < 4:
            self.logger.experiment.add_audio(
                f"Test/Mixture_{batch_idx}",
                mixture / mixture.abs().max(),
                self.current_epoch,
                sample_rate=self.config["global sr"],
            )
            for i, source in enumerate(reconstructed_sources):
                print(source.shape)
                self.logger.experiment.add_audio(
                    f"Test/Reconstruction_{batch_idx}{i}",
                    source / source.abs().max(),
                    self.current_epoch,
                    sample_rate=self.config["global sr"],
                )

        # Test to reconstruct the sources with the wrong dictionary
        wrong_dict_outputs = self.forward_wrong_dict(
            self.get_input(batch),
            batch["speakers"],
            batch["num_frames"],
        )

        return {
            "activations": activation_vector,
            "sources": sources,
            "reconstructions": reconstructed_sources,
            **wrong_dict_outputs,
        }

    def reconstruct(self, batch, outputs) -> list[torch.Tensor]:
        # Unpack arguments
        predictions_frames = outputs["predictions"]
        num_frames = batch["num_frames"]
        input_phases = batch["phases"]

        output_specs = self.transform.unstack(predictions_frames, num_frames)
        # Reconstruct audio
        output_audios = [
            self.transform.inverse(spec, phase.to(spec))
            for spec, phase in zip(output_specs, input_phases)
        ]

        for a in output_audios:
            a = a / torch.max(torch.abs(a))

        return output_audios

    def debug_print(self):
        for component in self.children():
            print(component)
        print(f"Dictionnary shape: {self.dictionary.shape}")


class ReconstructedAudioLogger(Callback):
    def __init__(self, sr: int, mini_batch: dict) -> None:
        super().__init__()
        self.sr = sr
        self.batch = mini_batch

    def log_audio(
        self,
        audio: torch.Tensor,
        name: str,
        trainer: Trainer,
    ) -> None:
        trainer.logger.experiment.add_audio(
            name,
            audio,
            trainer.global_step,
            sample_rate=self.sr,
        )

    def on_train_start(
        self,
        trainer: Trainer,
        pl_module: SDM_VAESpeakerSpecificDict,
    ) -> None:
        for i, audio in enumerate(self.batch["audios"]):
            self.log_audio(audio, f"Input/{i}", trainer)

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAESpeakerSpecificDict,
    ) -> None:
        samples = pl_module.get_input(self.batch)

        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)

        speakers = self.batch["speakers"]
        num_frames = self.batch["num_frames"]
        outputs = pl_module(samples, speakers, num_frames)

        output_audios = pl_module.reconstruct(self.batch, outputs)
        for i, audio in enumerate(output_audios):
            self.log_audio(audio, f"Reconstruction/{i}", trainer)


class DictionaryLogger(Callback):
    def __init__(self) -> None:
        super().__init__()

    def log_dictionary(
        self, model: SDM_VAESpeakerSpecificDict, trainer: Trainer
    ) -> None:
        dictionaries = model.dictionary[:3]
        for i, dictionary in enumerate(dictionaries):
            trainer.logger.experiment.add_image(
                f"Dictionary/{i}",
                dictionary.unsqueeze(0),
                trainer.global_step,
            )

    def on_train_start(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        self.log_dictionary(pl_module, trainer)

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        self.log_dictionary(pl_module, trainer)


class AudioMetrics(Callback):
    def __init__(self, config_m: dict, mini_batch: dict) -> None:
        super().__init__()
        self.batch = mini_batch
        metrics_list = config_m["audio_metrics"]
        self.metrics = {}
        for m in metrics_list:
            assert m in supported_audio_metrics, f"{m} is not supported."
            assert (
                m in config_m
            ), f"{m} is not in the config file. Add the appropriate arguments."
            init_args = config_m[m]
            instance = supported_audio_metrics[m](**init_args)
            self.metrics[m] = instance

    def compute_scores(self, ground_truths, predictions):
        scores = {}

        for m in self.metrics:
            value = 0
            for g, p in zip(ground_truths, predictions):
                g = g.to(p)
                value += self.metrics[m](target=g, preds=p)
            scores[m] = value / len(ground_truths)
        return scores

    def log_scores(self, scores: dict, name: str, trainer: Trainer):
        for m in scores:
            trainer.logger.experiment.add_scalar(
                f"{name}/{m}",
                scores[m],
                trainer.global_step,
            )

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAESpeakerSpecificDict,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)

        samples = pl_module.get_input(self.batch)
        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)

        in_audios = self.batch["audios"]

        speakers = self.batch["speakers"]
        num_frames = self.batch["num_frames"]
        outputs = pl_module(samples, speakers, num_frames)

        predictions_audios = pl_module.reconstruct(self.batch, outputs)
        scores = self.compute_scores(in_audios, predictions_audios)
        self.log_scores(scores, "Audio reconstruction", trainer)

    def on_test_start(
        self,
        trainer: Trainer,
        pl_module: SDM_VAESpeakerSpecificDict,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)

    def on_test_batch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAESpeakerSpecificDict,
        outputs: STEP_OUTPUT | None,
        batch: Any,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        ground_truths = outputs["sources"]
        predictions = outputs["reconstructions"]
        wrong_dict_predictions = outputs["predictions_wrong_dict"]
        scores = self.compute_scores(ground_truths, predictions)
        self.log_scores(scores, "Test/", trainer)
        scores = self.compute_scores(ground_truths, wrong_dict_predictions)
        self.log_scores(scores, "Test/Wrong_dict", trainer)


class SparseMetrics(Callback):
    def __init__(self, config_m: dict, mini_batch: dict) -> None:
        super().__init__()
        self.batch = mini_batch
        metrics_list = config_m["sparse_metrics"]
        self.metrics = {}
        for m in metrics_list:
            assert m in supported_sparse_metrics, f"{m} is not supported."
            assert (
                m in config_m
            ), f"{m} is not in the config file. Add the appropriate arguments."
            init_args = config_m[m]
            instance = supported_sparse_metrics[m](**init_args)
            self.metrics[m] = instance

    def compute_scores(self, vectors: torch.Tensor):
        scores = {}
        for m in self.metrics:
            scores[m] = self.metrics[m](vectors)
        return scores

    def log_scores(self, scores: dict, name: str, trainer: Trainer):
        for m in scores:
            trainer.logger.experiment.add_scalar(
                f"{name}/{m}",
                scores[m],
                trainer.global_step,
            )

    def on_validation_epoch_end(
        self, trainer: Trainer, pl_module: SDM_VAESpeakerSpecificDict
    ) -> None:
        samples = pl_module.get_input(self.batch)
        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)

        speakers = self.batch["speakers"]
        num_frames = self.batch["num_frames"]
        outputs = pl_module(samples, speakers, num_frames)

        vectors = outputs["sparse_vector"]
        scores = self.compute_scores(vectors)
        self.log_scores(scores, "Sparsity", trainer)

    def on_test_batch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
        outputs: STEP_OUTPUT | None,
        batch: Any,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        vectors = outputs["activations"]
        scores = self.compute_scores(vectors)
        self.log_scores(scores, "Test", trainer)
