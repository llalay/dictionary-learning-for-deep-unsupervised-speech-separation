from SDM_VAE_src.models import utils
from pytorch_lightning import LightningModule
import torch.nn as nn, torch


class AE(LightningModule):
    # All methods marked as 'built in' are automatically called by pytorch lightning
    def __init__(
        self,
        config: dict = None,
    ) -> None:
        super().__init__()
        self.save_hyperparameters()

        # Different Layers of the VAE
        self.config = config
        config_encoder, config_decoder = self.init_encoder_decoder()
        self.encoder = utils.Encoder(**config_encoder)
        self.decoder = utils.Decoder(**config_decoder)

        self.input_dim = config["network"]["input_dim"]
        self.lr = config["optimizer"]["learning_rate"]

        # Loss
        self.expectation_loss = utils.ExpectationLoss()
        self.debug_print()

    def init_encoder_decoder(self):
        config_n = self.config["network"]
        match config_n["activation"].lower():
            case "relu":
                activation = nn.ReLU()
            case "tanh":
                activation = nn.Tanh()
            case _:
                print("Use ReLU by default")
                activation = nn.ReLU()
        match config_n["mode"]:
            case "linear":
                config_encoder = {
                    "input_dim": config_n["input_dim"],
                    "hidden_dims": config_n["linear"]["hidden_dims"]
                    + [config_n["latent_dim"]],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
                config_decoder = {
                    "latent_dim": config_n["latent_dim"],
                    "hidden_dims": list(reversed(config_n["linear"]["hidden_dims"])),
                    "output_dim": config_n["input_dim"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
            case _:
                print("Wrong mode, try linear instead")
        return config_encoder, config_decoder

    def get_input(self, batch):
        """Modify this function according to the collate function of the dataloader"""
        return batch["input"]

    def forward(self, x) -> dict:
        # Built in

        # Encoding
        encoded_vector = self.encoder(x)

        # Reconstruction
        predictions = torch.exp(self.decoder(encoded_vector))

        return {
            "predictions": predictions,
            "activations": encoded_vector,
        }

    def configure_optimizers(self):
        # Built in
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def training_step(self, batch, batch_idx):
        # Built in

        samples = self.get_input(batch)
        outputs = self(samples)
        batch_size = samples.shape[0]

        loss = self.expectation_loss(
            predictions=outputs["predictions"],
            target=samples,
        )
        self.log("loss/train_loss", loss, batch_size=batch_size)

        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        # Built in. Model is automatically set to eval and no grad mode

        samples = self.get_input(batch)
        outputs = self(samples)
        batch_size = samples.shape[0]

        loss = self.expectation_loss(
            predictions=outputs["predictions"],
            target=samples,
        )
        self.log("loss/val_loss", loss, batch_size=batch_size)

        outputs["loss"] = loss
        outputs["ground_truth"] = samples

        return outputs

    def test_step(self, batch, batch_idx):
        samples = self.get_input(batch)
        outputs = self(samples)
        outputs["ground_truth"] = samples

        return outputs

    def debug_print(self):
        for component in self.children():
            print(component)
