import torch, pytorch_lightning as pl, copy
from SDM_VAE_src.models import utils
from SDM_VAE_src.dataprocessing import SourceSeparationSpecTransform
from SDM_VAE_src.metrics import PESQ, STOI, SI_SDR, SI_SNR, HoyerSparsity
from typing_extensions import TypedDict, NotRequired
from config import ConfigClass
from torch import nn

# Path: SDM_VAE_src/models/sdm_clustering.py


class BatchDictSpecific(TypedDict):
    input: torch.Tensor
    ground_truth: torch.Tensor
    num_frames: list[int]
    speakers: list[tuple[str]]
    audios: NotRequired[list[torch.Tensor]]
    sources_audios: NotRequired[list[torch.Tensor]]
    phases: NotRequired[list[torch.Tensor]]


class StepOutput(TypedDict):
    loss: torch.Tensor
    reconstructed_sources: list[torch.Tensor]
    sparse_vector: torch.Tensor


class SupervisedDictSpecific(pl.LightningModule):
    def __init__(
        self,
        config: ConfigClass,
        speakers: list[str],
        transform: SourceSeparationSpecTransform,
    ) -> None:
        """SDM Clustering model

        Input
        -----
            config: ConfigClass
                Configuration dictionary from YAML file
            speakers: list[str]
                List of speakers ID in the dataset
        """
        super().__init__()

        self.save_hyperparameters(ignore="transform")
        self.config = config
        self.transform = copy.deepcopy(transform)

        n_bins = self.config["transform"]["STFT"]["n_fft"] // 2 + 1
        self.input_dim = n_bins

        # Autoencoder
        config_encoder, config_decoder = self.init_encoder_decoder()
        self.encoder = utils.Encoder(**config_encoder)
        self.decoder = utils.Decoder(**config_decoder)

        # Variational part
        self.mean = nn.Linear(
            self.encoder.encode_dim,
            self.config["network"]["dict_dim"],
        )
        self.logvar = nn.Linear(
            self.encoder.encode_dim,
            self.config["network"]["dict_dim"],
        )

        num_speaker = len(speakers)
        self.speaker2id = {s: i for i, s in enumerate(speakers)}

        dictionaries = torch.rand(
            num_speaker,
            config["network"]["latent_dim"],
            config["network"]["dict_dim"],
        )
        if not config["network"]["random_dict"]:
            for k in range(num_speaker):
                dictionaries[k] = utils.create_dictionary_dct(
                    config["network"]["latent_dim"],
                    config["network"]["dict_dim"],
                )
        self.dictionary = nn.Parameter(dictionaries, requires_grad=True)

        self.time_loss = nn.L1Loss()
        self.reparametrization = utils.GaussianReparametrization()

        self.init_metrics()

    def init_encoder_decoder(self):
        config_n = self.config["network"]
        match config_n["activation"].lower():
            case "relu":
                activation = nn.ReLU()
            case "tanh":
                activation = nn.Tanh()
            case _:
                print("Use ReLU by default")
                activation = nn.ReLU()
        match config_n["mode"]:
            case "linear":
                config_encoder = {
                    "input_dim": self.input_dim,
                    "hidden_dims": config_n["linear"]["hidden_dims"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
                config_decoder = {
                    "latent_dim": config_n["latent_dim"],
                    "hidden_dims": list(reversed(config_n["linear"]["hidden_dims"])),
                    "output_dim": self.input_dim,
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
            case _:
                print("Wrong mode, try linear instead")
        return config_encoder, config_decoder

    def init_metrics(self):
        self.pesq = PESQ(**self.config["metrics"]["pesq"])
        self.stoi = STOI(**self.config["metrics"]["stoi"])
        self.si_sdr = SI_SDR(**self.config["metrics"]["si_sdr"])
        self.si_snr = SI_SNR(**self.config["metrics"]["si_snr"])
        self.hoyer = HoyerSparsity(**self.config["metrics"]["hoyer"])

    def loss(
        self,
        reconstructed_sources: list[torch.Tensor],
        true_sources: list[torch.Tensor],
    ):
        loss = 0
        for estimate, truth in zip(reconstructed_sources, true_sources):
            loss = loss + self.time_loss(estimate, truth)

        # Normalize by the number of signals
        loss = loss / len(reconstructed_sources)
        return loss

    def forward(
        self,
        x: torch.Tensor,
        phase: torch.Tensor,
        sources: torch.Tensor,
        speakers: list[tuple[str]],
        num_frames: list[int],
    ) -> torch.Tensor:
        # x.shape = (N_FRAMES, N_BINS)
        # encoded_vector.shape = (N_FRAMES, ENCODE_DIM)
        encoded_vector = self.encoder(x)
        # mean.shape = logvar.shape = (N_FRAMES, DICT_DIM)
        mean = self.mean(encoded_vector)
        logvar = self.logvar(encoded_vector)

        # activation_vector.shape = (N_FRAMES, DICT_DIM)
        activation_vector = self.reparametrization(mean, logvar)

        # Each frame is the mixture of multiple speakers
        # Compute one latent space per speaker with the corresponding dictionary

        start = 0
        reconstructed_sources = [torch.zeros_like(s) for s in sources]
        for i, mix_speakers in enumerate(speakers):
            # speaker_activation_frames.shape = (NUM_FRAMES[i], DICT_DIM)
            end = start + num_frames[i]
            speaker_activation_frames = activation_vector[start:end]
            start = end

            for j, s in enumerate(mix_speakers):
                # dictionary.shape = (LATENT_DIM, DICT_DIM)
                dictionary = self.dictionary[self.speaker2id[s]]

                # latent_vector.shape = (NUM_FRAMES[i], LATENT_DIM)
                latent_vector = speaker_activation_frames @ dictionary.T

                # Decode, Reconstruct and compute the separation loss for each speaker
                decoded_vector = torch.exp(self.decoder(latent_vector))
                reconstructed_sources[i][j] = self.reconstruct(decoded_vector, phase[i])

        return reconstructed_sources, activation_vector

    def training_step(self, batch: BatchDictSpecific, batch_idx: int) -> torch.Tensor:
        reconstructed_sources, activation_vector = self.forward(
            x=batch["input"],
            phase=batch["phases"],
            sources=batch["sources_audios"],
            speakers=batch["speakers"],
            num_frames=batch["num_frames"],
        )
        train_loss = self.loss(reconstructed_sources, batch["sources_audios"])
        self.log("loss/train_loss", train_loss, batch_size=len(batch["input"]))

        return train_loss

    def validation_step(self, batch: BatchDictSpecific, batch_idx: int) -> torch.Tensor:
        reconstructed_sources, activation_vector = self(
            batch["input"],
            batch["phases"],
            batch["sources_audios"],
            batch["speakers"],
            batch["num_frames"],
        )

        val_loss = self.loss(reconstructed_sources, batch["sources_audios"])
        self.log("loss/val_loss", val_loss, batch_size=len(batch["input"]))

        return StepOutput(
            loss=val_loss,
            reconstructed_sources=reconstructed_sources,
            sparse_vector=activation_vector,
        )

    def on_validation_batch_end(
        self,
        outputs: StepOutput,
        batch: BatchDictSpecific,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        # Log one batch only for speed purpose
        if batch_idx == 0:
            for i, (estimates, truth) in enumerate(
                zip(outputs["reconstructed_sources"], batch["sources_audios"])
            ):
                self.log_separation_metrics(truth, estimates, legend="val_metrics")
                self.log_speech_metrics(truth, estimates, legend="val_metrics")
                for j in range(len(estimates)):
                    self.log_audio(estimates[j], legend=f"val_audio/estimate_{i}{j}")
                    self.log_audio(truth[j], legend=f"val_audio/truth_{i}{j}")

        self.log_sparse_metrics(outputs["sparse_vector"], legend="val_metrics")

    def test_step(self, batch: BatchDictSpecific, batch_idx: int) -> torch.Tensor:
        reconstructed_sources, activation_vector = self(
            batch["input"],
            batch["phases"],
            batch["sources_audios"],
            batch["speakers"],
            batch["num_frames"],
        )

        return reconstructed_sources

    def configure_optimizers(self) -> torch.optim.Optimizer:
        return torch.optim.Adam(
            self.parameters(),
            lr=self.config["optimizer"]["learning_rate"],
        )

    def reconstruct(self, transposed_spec: torch.Tensor, phase: torch.Tensor):
        # spec.shape = (N_FRAMES, N_BINS)
        return self.transform.inverse(torch.transpose(transposed_spec, 0, 1), phase)

    def log_audio(self, audio: torch.Tensor, legend: str):
        self.logger.experiment.add_audio(
            legend,
            audio / audio.abs().max(),
            self.current_epoch,
            sample_rate=self.config["global sr"],
        )

    def log_speech_metrics(
        self,
        target: torch.Tensor,
        prediction: torch.Tensor,
        legend: str = "metrics",
    ):
        pesq = self.pesq(target=target, preds=prediction)
        stoi = self.stoi(target=target, preds=prediction)

        self.log(f"{legend}/pesq", pesq, batch_size=len(target))
        self.log(f"{legend}/stoi", stoi, batch_size=len(target))

    def log_separation_metrics(
        self,
        target: torch.Tensor,
        prediction: torch.Tensor,
        legend: str = "metrics",
    ):
        si_sdr = self.si_sdr(target=target, preds=prediction)
        si_snr = self.si_snr(target=target, preds=prediction)

        self.log(f"{legend}/si_sdr", si_sdr, batch_size=len(target))
        self.log(f"{legend}/si_snr", si_snr, batch_size=len(target))

    def log_sparse_metrics(self, vector: torch.Tensor, legend: str = "metrics"):
        hoyer = self.hoyer(vector)
        self.log(f"{legend}/hoyer", hoyer, batch_size=len(vector))
