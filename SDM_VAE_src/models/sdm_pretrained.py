from pathlib import Path
import torch, pytorch_lightning as pl, copy
from SDM_VAE_src.models import utils
from SDM_VAE_src.metrics import PESQ, STOI, SI_SDR, SI_SNR, HoyerSparsity
from SDM_VAE_src.dataprocessing import SourceSeparationSpecTransform
from typing import Any
from SDM_VAE_src.models.sdm_supervised_dict_specific import (
    BatchDictSpecific,
    StepOutput,
)
from config import ConfigClass
from torch import nn

# Path: SDM_VAE_src/models/sdm_clustering.py


class SDMPretrained(pl.LightningModule):
    def __init__(
        self,
        config: ConfigClass,
        speakers: list[str],
        transform: SourceSeparationSpecTransform,
        checkpoint: Path,
    ) -> None:
        super().__init__()

        self.save_hyperparameters(ignore="transform")
        self.config = config
        self.transform = copy.deepcopy(transform)

        # Load the VAE part from the trained model
        trained_state_dict = torch.load(checkpoint)["state_dict"]
        self.encoder = self.module_from_state_dict(trained_state_dict, "encoder")
        self.decoder = self.module_from_state_dict(trained_state_dict, "decoder")
        self.mean = self.module_from_state_dict(trained_state_dict, "mean")
        self.logvar = self.module_from_state_dict(trained_state_dict, "log_var")

        # Create the dictionaries
        num_speaker = len(speakers)
        self.speaker2id = {s: i for i, s in enumerate(speakers)}

        dictionaries = torch.rand(
            num_speaker,
            config["network"]["latent_dim"],
            config["network"]["dict_dim"],
        )
        if not config["network"]["random_dict"]:
            for k in range(num_speaker):
                dictionaries[k] = utils.create_dictionary_dct(
                    config["network"]["latent_dim"],
                    config["network"]["dict_dim"],
                )
        self.dictionary = nn.Parameter(dictionaries, requires_grad=True)

        self.time_loss = nn.L1Loss()
        self.reparametrization = utils.GaussianReparametrization()
        self.init_metrics()
        self.debug_print()

    def init_metrics(self):
        self.pesq = PESQ(**self.config["metrics"]["pesq"])
        self.stoi = STOI(**self.config["metrics"]["stoi"])
        self.si_sdr = SI_SDR(**self.config["metrics"]["si_sdr"])
        self.si_snr = SI_SNR(**self.config["metrics"]["si_snr"])
        self.hoyer = HoyerSparsity(**self.config["metrics"]["hoyer"])

    def module_from_state_dict(self, state_dict: dict[str], key: str) -> nn.Sequential:
        module_keys = [k for k in state_dict.keys() if k.startswith(key)]
        weights = [k for k in module_keys if k.endswith("weight")]
        weights.sort()
        biases = [k for k in module_keys if k.endswith("bias")]
        biases.sort()
        module = nn.Sequential()
        for w, b in zip(weights, biases):
            weights = nn.Parameter(state_dict[w])
            biases = nn.Parameter(state_dict[b])
            linear = nn.Linear(weights.shape[1], weights.shape[0])
            linear.weight, linear.bias = weights, biases
            module.append(linear)
            module.append(nn.Tanh())

        if key != "encoder":
            # Remove the last activation
            module = module[:-1]
        return module

    def configure_optimizers(self) -> torch.optim.Optimizer:
        return torch.optim.Adam(
            self.parameters(),
            lr=self.config["optimizer"]["learning_rate"],
        )

    def loss(
        self,
        reconstructed_sources: list[torch.Tensor],
        true_sources: list[torch.Tensor],
    ):
        loss = 0
        for estimate, truth in zip(reconstructed_sources, true_sources):
            loss = loss + self.time_loss(estimate, truth)

        # Normalize by the number of signals
        loss = loss / len(reconstructed_sources)
        return loss

    def forward(
        self,
        x: torch.Tensor,
        phase: torch.Tensor,
        sources: torch.Tensor,
        speakers: list[tuple[str]],
        num_frames: list[int],
    ) -> torch.Tensor:
        # x.shape = (N_FRAMES, N_BINS)
        # encoded_vector.shape = (N_FRAMES, ENCODE_DIM)
        encoded_vector = self.encoder(x)
        # mean.shape = logvar.shape = (N_FRAMES, DICT_DIM)
        mean = self.mean(encoded_vector)
        logvar = self.logvar(encoded_vector)

        # activation_vector.shape = (N_FRAMES, DICT_DIM)
        activation_vector = self.reparametrization(mean, logvar)

        # Each frame is the mixture of multiple speakers
        # Compute one latent space per speaker with the corresponding dictionary

        start = 0
        reconstructed_sources = [torch.zeros_like(s) for s in sources]
        for i, mix_speakers in enumerate(speakers):
            # speaker_activation_frames.shape = (NUM_FRAMES[i], DICT_DIM)
            end = start + num_frames[i]
            speaker_activation_frames = activation_vector[start:end]
            start = end

            for j, s in enumerate(mix_speakers):
                # dictionary.shape = (LATENT_DIM, DICT_DIM)
                dictionary = self.dictionary[self.speaker2id[s]]

                # latent_vector.shape = (NUM_FRAMES[i], LATENT_DIM)
                latent_vector = speaker_activation_frames @ dictionary.T

                # Decode, Reconstruct and compute the separation loss for each speaker
                decoded_vector = torch.exp(self.decoder(latent_vector))
                reconstructed_sources[i][j] = self.reconstruct(decoded_vector, phase[i])

        return reconstructed_sources, activation_vector

    def training_step(self, batch: BatchDictSpecific, batch_idx: int) -> torch.Tensor:
        reconstructed_sources, activation_vector = self.forward(
            x=batch["input"],
            phase=batch["phases"],
            sources=batch["sources_audios"],
            speakers=batch["speakers"],
            num_frames=batch["num_frames"],
        )
        train_loss = self.loss(reconstructed_sources, batch["sources_audios"])
        self.log("loss/train_loss", train_loss, batch_size=len(batch["input"]))

        return train_loss

    def validation_step(self, batch: BatchDictSpecific, batch_idx: int) -> torch.Tensor:
        reconstructed_sources, activation_vector = self(
            batch["input"],
            batch["phases"],
            batch["sources_audios"],
            batch["speakers"],
            batch["num_frames"],
        )

        val_loss = self.loss(reconstructed_sources, batch["sources_audios"])
        self.log("loss/val_loss", val_loss, batch_size=len(batch["input"]))

        return StepOutput(
            loss=val_loss,
            reconstructed_sources=reconstructed_sources,
            sparse_vector=activation_vector,
        )

    def on_validation_batch_end(
        self,
        outputs: StepOutput,
        batch: BatchDictSpecific,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        # Log one batch only for speed purpose
        if batch_idx == 0:
            for i, (estimates, truth) in enumerate(
                zip(outputs["reconstructed_sources"], batch["sources_audios"])
            ):
                self.log_separation_metrics(truth, estimates, legend="val_metrics")
                self.log_speech_metrics(truth, estimates, legend="val_metrics")
                for j in range(len(estimates)):
                    self.log_audio(estimates[j], legend=f"val_audio/estimate_{i}{j}")
                    self.log_audio(truth[j], legend=f"val_audio/truth_{i}{j}")

        self.log_sparse_metrics(outputs["sparse_vector"], legend="val_metrics")

    def test_step(self, batch: BatchDictSpecific, batch_idx: int) -> torch.Tensor:
        reconstructed_sources, activation_vector = self(
            batch["input"],
            batch["phases"],
            batch["sources_audios"],
            batch["speakers"],
            batch["num_frames"],
        )

        return reconstructed_sources

    def reconstruct(self, transposed_spec: torch.Tensor, phase: torch.Tensor):
        # spec.shape = (N_FRAMES, N_BINS)
        return self.transform.inverse(torch.transpose(transposed_spec, 0, 1), phase)

    def log_audio(self, audio: torch.Tensor, legend: str):
        self.logger.experiment.add_audio(
            legend,
            audio / audio.abs().max(),
            self.current_epoch,
            sample_rate=self.config["global sr"],
        )

    def log_speech_metrics(
        self,
        target: torch.Tensor,
        prediction: torch.Tensor,
        legend: str = "metrics",
    ):
        pesq = self.pesq(target=target, preds=prediction)
        stoi = self.stoi(target=target, preds=prediction)

        self.log(f"{legend}/pesq", pesq, batch_size=len(target))
        self.log(f"{legend}/stoi", stoi, batch_size=len(target))

    def log_separation_metrics(
        self,
        target: torch.Tensor,
        prediction: torch.Tensor,
        legend: str = "metrics",
    ):
        si_sdr = self.si_sdr(target=target, preds=prediction)
        si_snr = self.si_snr(target=target, preds=prediction)

        self.log(f"{legend}/si_sdr", si_sdr, batch_size=len(target))
        self.log(f"{legend}/si_snr", si_snr, batch_size=len(target))

    def log_sparse_metrics(self, vector: torch.Tensor, legend: str = "metrics"):
        hoyer = self.hoyer(vector)
        self.log(f"{legend}/hoyer", hoyer, batch_size=len(vector))

    def debug_print(self):
        for c in self.children():
            print(c)


def main():
    log_dir = Path("/home/louis/Nextcloud/Stage/Logs/good_logs")
    log_1 = log_dir / "TCD-SdmSupervisedDictSpecific-Z64xD128-TimeLoss-7"
    ckpt = log_1 / "checkpoints" / "last.ckpt"
    loaded_ckpt = torch.load(ckpt)
    state_dict = loaded_ckpt["state_dict"]

    with open("config/_!test.yml") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    transform = SourceSeparationSpecTransform(config["transform"])
    dm = TCDMixDatamodule(config, transform=transform)

    model = SDMPretrained(
        config=config,
        speakers=dm.speakers,
        transform=copy.deepcopy(transform),
        trained_state_dict=state_dict,
    )

    trainer = pl.Trainer()
    trainer.fit(model, dm)


if __name__ == "__main__":
    from SDM_VAE_src.dataprocessing import TCDMixDatamodule
    import yaml, copy

    main()
