import torch.nn as nn
import numpy as np
import torch


def create_linear_layers(
    dimensions: list,
    activation_function,
    dropout: float,
    last_layer_activation: bool = False,
):
    """
    Create a linear layer with the given dimensions and activation function.

    Args
    ----
        dimensions: list
            List of dimensions of the linear layers
        activation_function: torch.nn.Module
            Activation function to be used in the layers
        dropout: float
            Dropout probability
        last_layer_activation: bool
            If True, the last layer will have the activation function applied to it

    Returns
    -------
        layers: torch.nn.Sequential
            Sequential layer with the given dimensions and activation function
    """
    # Create layer from the given dimensions
    layers = nn.Sequential()
    for i in range(len(dimensions) - 1):
        layers.append(nn.Linear(dimensions[i], dimensions[i + 1]))
        if last_layer_activation or (i < len(dimensions) - 2):
            layers.append(activation_function)
            if not (np.isclose(dropout, 0.0)):
                layers.append(nn.Dropout(p=dropout))
    return layers


class Encoder(nn.Module):
    """Encoder class. Currently support linear network"""

    def __init__(self, **kwargs):
        super().__init__()
        self.encode_dim = None
        self.init_linear(**kwargs)

    def init_linear(self, **kwargs):
        layer_dims = [kwargs["input_dim"]] + kwargs["hidden_dims"]
        self.encode_dim = layer_dims[-1]
        self.encode = create_linear_layers(
            layer_dims,
            kwargs["activation_function"],
            kwargs["dropout"],
            last_layer_activation=True,
        )

    def forward(self, x):
        return self.encode(x)


class Decoder(nn.Module):
    """Decoder class. Currently support linear network"""

    def __init__(self, **kwargs):
        super().__init__()
        self.init_linear(**kwargs)

    def init_linear(self, **kwargs):
        layer_dims = (
            [kwargs["latent_dim"]] + kwargs["hidden_dims"] + [kwargs["output_dim"]]
        )
        self.decode = create_linear_layers(
            layer_dims, kwargs["activation_function"], kwargs["dropout"]
        )

    def forward(self, x):
        return self.decode(x)


def create_dictionary_dct(latent_dim: int, dict_size: int):
    """Create discrete cosine transform dictionary used in the latent space factorization"""
    latent_dim = torch.tensor(latent_dim)
    dictionary = torch.ones((latent_dim, dict_size)) / torch.sqrt(latent_dim)
    for i in range(1, dict_size):
        time = torch.arange(latent_dim)
        column = torch.cos(time * np.pi * i / dict_size)
        column = column - torch.mean(column)
        column = column / torch.linalg.norm(column, 2)
        dictionary[:, i] = column
        dictionary.requires_grad = False
    return dictionary


class GaussianKLDivLoss(nn.Module):
    """Computes the KL divergence between two Gaussian distributions as follows:
    KLDiv[ p_target || q_predic ]

    Args
    ----
        mean_target: torch.Tensor
            Mean of the target distribution
        var_target: torch.Tensor
            Variance of the target distribution
        mean_predic: torch.Tensor
            Mean of the predicted distribution
        var_predic: torch.Tensor
            Variance of the predicted distribution
    Returns
    -------
        KL_div_loss: torch.Tensor
            KL divergence between the two distributions
    """

    def __init__(self):
        super().__init__()

    def forward(self, mean_current, var_current, mean_prior, var_prior):
        batch_size = mean_current.shape[0]
        KLD = 0.5 * (
            (var_current + torch.square(mean_current - mean_prior)) / var_prior
            - torch.log(var_current / var_prior)
            - 1
        )

        return torch.sum(KLD) / batch_size


class GaussianReparametrization(nn.Module):
    """Reparametrization trick to sample from a Gaussian distribution"""

    def __init__(self):
        super().__init__()

    def forward(self, mean, log_var):
        std = torch.exp(0.5 * log_var)
        eps = torch.randn_like(std)

        return torch.addcmul(mean, eps, std)


class ExpectationLoss(nn.Module):
    """Computes the expectation loss of the ELBO."""

    def __init__(self):
        super().__init__()

    def forward(self, target, predictions):
        batch_size = target.shape[0]
        expectation = target / (predictions) + torch.log(predictions)

        return torch.sum(expectation) / batch_size


class L1Loss(nn.Module):
    """Computes the L1 loss between two tensors."""

    def __init__(self):
        super().__init__()

    def forward(
        self,
        target: list[torch.Tensor] | torch.Tensor,
        predictions: list[torch.Tensor] | torch.Tensor,
    ):
        assert len(target) == len(
            predictions
        ), "Target and predictions must have the same length"

        # If only one tensor is given, compute the L1 loss between the two tensors
        if isinstance(target, torch.Tensor) and isinstance(predictions, torch.Tensor):
            return torch.sum(torch.abs(target - predictions))

        # For a list or multiple tensors, compute the mean of the L1 loss between the tensors
        loss = 0
        for i in range(len(target)):
            loss = loss + torch.sum(torch.abs(target[i] - predictions[i]))
        return loss / len(target)
