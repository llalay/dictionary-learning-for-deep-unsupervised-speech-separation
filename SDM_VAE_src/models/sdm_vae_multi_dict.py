import torch.nn as nn, torch, copy
from typing import Any
from pytorch_lightning import Callback, LightningModule, Trainer
from pytorch_lightning.utilities.types import STEP_OUTPUT
from SDM_VAE_src.dataprocessing.transforms import SourceSeparationSpecTransform
from SDM_VAE_src.models import utils
from SDM_VAE_src.metrics import supported_audio_metrics, supported_sparse_metrics


class SDM_VAEMultiDict(LightningModule):
    # All methods marked as 'built in' are automatically called by pytorch lightning
    def __init__(
        self,
        config: dict = None,
        mini_batch: dict = None,
        transform: SourceSeparationSpecTransform = None,
    ) -> None:
        super().__init__()
        self.save_hyperparameters(ignore="transform")

        self.transform = copy.deepcopy(transform)

        # Different Layers of the VAE
        self.config = config
        self.num_speaker = config["global num_speaker"]
        config_encoder, config_decoder = self.init_encoder_decoder()
        self.encoder = utils.Encoder(**config_encoder)
        self.decoder = utils.Decoder(**config_decoder)
        self.log_var = nn.Linear(
            self.encoder.encode_dim,
            self.num_speaker * config["network"]["dict_dim"],
        )
        self.mean = nn.Linear(
            self.encoder.encode_dim,
            self.num_speaker * config["network"]["dict_dim"],
        )

        self.input_dim = config["network"]["input_dim"]
        self.lr = config["optimizer"]["learning_rate"]

        dictionaries = torch.rand(
            self.num_speaker,
            config["network"]["latent_dim"],
            config["network"]["dict_dim"],
        )
        self.dictionary = nn.Parameter(dictionaries, requires_grad=True)

        # Losses
        self.expectation_loss = utils.ExpectationLoss()
        self.kl_loss = utils.GaussianKLDivLoss()
        self.time_domain_loss = utils.L1Loss()
        self.beta = config["training"]["beta"]
        self.repametrization = utils.GaussianReparametrization()

        self.init_callbacks(mini_batch)

    def init_encoder_decoder(self):
        config_n = self.config["network"]
        match config_n["activation"].lower():
            case "relu":
                activation = nn.ReLU()
            case "tanh":
                activation = nn.Tanh()
            case _:
                print("Use ReLU by default")
                activation = nn.ReLU()
        match config_n["mode"]:
            case "linear":
                config_encoder = {
                    "input_dim": config_n["input_dim"],
                    "hidden_dims": config_n["linear"]["hidden_dims"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
                config_decoder = {
                    "latent_dim": config_n["latent_dim"],
                    "hidden_dims": list(reversed(config_n["linear"]["hidden_dims"])),
                    "output_dim": config_n["input_dim"],
                    "activation_function": activation,
                    "dropout": config_n["dropout"],
                }
            case _:
                print("Wrong mode, try linear instead")
        return config_encoder, config_decoder

    def init_callbacks(self, mini_batch: dict = None):
        # Initiliaze model specific callbacks
        self.callbacks = []

        log_audio = ReconstructedAudioLogger(self.config["global sr"], mini_batch)
        self.callbacks.append(log_audio)

        log_dict = DictionaryLogger()
        self.callbacks.append(log_dict)

        log_sparse = SparseMetrics(self.config["metrics"], mini_batch)
        self.callbacks.append(log_sparse)

        log_audio_metrics = AudioMetrics(self.config["metrics"], mini_batch)
        self.callbacks.append(log_audio_metrics)

    def get_input(self, batch):
        """Modify this function according to the collate function of the dataloader"""
        return batch["input"]

    def forward(self, x) -> dict:
        # Built in

        # Variational encoding
        encoded_vector = self.encoder(x)
        mean = self.mean(encoded_vector)
        log_var = self.log_var(encoded_vector)

        # Update Prior distribution
        with torch.no_grad():
            var_prior = torch.exp(log_var) + torch.square(mean)

        # Reparametrization trick
        activation_vector: torch.Tensor = self.repametrization(mean, log_var)

        # Reshape the activation vector to account for the different speakers
        activation_vector = activation_vector.view(
            x.shape[0],
            self.num_speaker,
            1,
            -1,
        )

        # Compute the latent vectors for each speaker
        latent_vectors = (
            activation_vector @ torch.transpose(self.dictionary, 1, 2)
        ).squeeze()

        # Reconstruct all the speakers
        predicted_source_specs = torch.exp(self.decoder(latent_vectors))

        return {
            "source_specs": torch.einsum("xyz->yxz", predicted_source_specs),
            "sparse_vector": activation_vector,
            "current_distribution": (mean, log_var),
            "prior_distribution": (torch.zeros_like(mean), var_prior),
        }

    def configure_optimizers(self):
        # Built in
        return torch.optim.Adam(self.parameters(), lr=self.lr)

    def loss(self, batch, outputs):
        config_m = self.config["model"]
        if "time" in config_m["loss_domain"]:
            reconstructed_sources = self.reconstruct(batch, outputs)
            reconstructions_loss = self.time_domain_loss(
                reconstructed_sources,
                batch["sources_audios"],
            )
        else:
            true_source_frames = batch["ground_truth"]
            reconstructions_loss = self.expectation_loss(
                true_source_frames, outputs["source_specs"]
            )

        self.log(
            "loss/reconstructions_loss",
            reconstructions_loss,
            batch_size=self.batch_size,
        )

        mean, log_var = outputs["current_distribution"]
        prior_mean, prior_var = outputs["prior_distribution"]
        kl_loss = self.kl_loss(mean, torch.exp(log_var), prior_mean, prior_var)

        self.log("loss/kl_loss", kl_loss, batch_size=self.batch_size)

        return reconstructions_loss + self.beta * kl_loss

    def training_step(self, batch, batch_idx):
        # Built in

        samples = self.get_input(batch)
        outputs = self(samples)
        self.batch_size = samples.shape[0]

        loss = self.loss(batch, outputs)
        self.log("loss/train_loss", loss, batch_size=self.batch_size)

        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        # Built in. Model is automatically set to eval and no grad mode

        samples = self.get_input(batch)
        outputs = self(samples)
        self.batch_size = samples.shape[0]

        loss = self.loss(batch, outputs)
        self.log("loss/val_loss", loss, batch_size=self.batch_size)

        outputs["loss"] = loss

        return outputs

    def test_step(self, batch, batch_idx):
        samples = self.get_input(batch)
        outputs = self(samples)
        outputs["ground_truth"] = samples

        return outputs

    def reconstruct(self, batch, outputs) -> list[torch.Tensor]:
        # Reconstruct the sources
        phase_mix: list[torch.Tensor] = batch["phases"]
        num_frames: list[int] = batch["num_frames"]
        sources_frames: torch.Tensor = outputs["source_specs"]

        source_specs = self.transform.unstack(sources_frames, num_frames)
        audios = [
            self.transform.inverse(s, p.to(s)) for s, p in zip(source_specs, phase_mix)
        ]

        return audios

    def debug_print(self):
        for component in self.children():
            print(component)
        print(f"Dictionnary shape: {self.dictionary.shape}")


class ReconstructedAudioLogger(Callback):
    def __init__(self, sr: int, mini_batch: dict) -> None:
        super().__init__()
        self.sr = sr
        self.batch = mini_batch

    def log_audio(
        self,
        audio: torch.Tensor,
        name: str,
        trainer: Trainer,
    ) -> None:
        trainer.logger.experiment.add_audio(
            name,
            audio,
            trainer.global_step,
            sample_rate=self.sr,
        )

    def on_train_start(
        self,
        trainer: Trainer,
        pl_module: SDM_VAEMultiDict,
    ) -> None:
        sources: list = self.batch["sources_audios"]
        for i, audio in enumerate(self.batch["audios"]):
            self.log_audio(audio, f"Input/{i}", trainer)
            for j in range(pl_module.num_speaker):
                self.log_audio(sources[i][j], f"Source/{i}/{j}", trainer)

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAEMultiDict,
    ) -> None:
        samples = pl_module.get_input(self.batch)

        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)

        outputs = pl_module(samples)

        reconstructed_sources = pl_module.reconstruct(self.batch, outputs)

        for i, audios in enumerate(reconstructed_sources):
            for j in range(pl_module.num_speaker):
                self.log_audio(audios[j], f"Reconstructed/{i}/{j}", trainer)


class DictionaryLogger(Callback):
    def __init__(self) -> None:
        super().__init__()

    def log_dictionary(self, model: SDM_VAEMultiDict, trainer: Trainer) -> None:
        dictionaries = model.dictionary
        for i, dictionary in enumerate(dictionaries):
            trainer.logger.experiment.add_image(
                f"Dictionary/{i}",
                dictionary.unsqueeze(0),
                trainer.global_step,
            )

    def on_train_start(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        self.log_dictionary(pl_module, trainer)

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: LightningModule,
    ) -> None:
        self.log_dictionary(pl_module, trainer)


class AudioMetrics(Callback):
    def __init__(self, config_m: dict, mini_batch: dict) -> None:
        super().__init__()
        self.batch = mini_batch
        metrics_list = config_m["audio_metrics"]
        self.metrics = {}
        for m in metrics_list:
            assert m in supported_audio_metrics, f"{m} is not supported."
            assert (
                m in config_m
            ), f"{m} is not in the config file. Add the appropriate arguments."
            init_args = config_m[m]
            instance = supported_audio_metrics[m](**init_args)
            self.metrics[m] = instance

    def compute_scores(
        self,
        true_sources: list[torch.Tensor],
        reconstructed_sources: list[torch.Tensor],
    ):
        scores = {}
        for m in self.metrics:
            value = 0
            for i, sources in enumerate(true_sources):
                value += self.metrics[m](
                    target=sources.to(reconstructed_sources[i]),
                    preds=reconstructed_sources[i],
                )
            scores[m] = value / len(true_sources)

        return scores

    def log_scores(self, scores: dict, name: str, trainer: Trainer) -> None:
        for m in scores:
            trainer.logger.experiment.add_scalar(
                f"{name}/{m}",
                scores[m],
                trainer.global_step,
            )

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAEMultiDict,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        samples = pl_module.get_input(self.batch)
        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)

        true_sources = self.batch["sources_audios"]
        reconstructed_sources = pl_module.reconstruct(self.batch, pl_module(samples))

        scores = self.compute_scores(true_sources, reconstructed_sources)
        self.log_scores(scores, "Source reconstruction", trainer)

    def on_test_batch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAEMultiDict,
        outputs: STEP_OUTPUT | None,
        batch: Any,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)

        true_sources = batch["sources_audios"]
        reconstructed_sources = pl_module.reconstruct(batch, outputs)

        scores = self.compute_scores(true_sources, reconstructed_sources)
        self.log_scores(scores, "Test", trainer)


class SparseMetrics(Callback):
    def __init__(self, config_m: dict, mini_batch: dict) -> None:
        super().__init__()
        self.batch = mini_batch
        metrics_list = config_m["sparse_metrics"]
        self.metrics = {}
        for m in metrics_list:
            assert m in supported_sparse_metrics, f"{m} is not supported."
            assert (
                m in config_m
            ), f"{m} is not in the config file. Add the appropriate arguments."
            init_args = config_m[m]
            instance = supported_sparse_metrics[m](**init_args)
            self.metrics[m] = instance

    def compute_scores(self, vectors: torch.Tensor):
        scores = {}
        for m in self.metrics:
            scores[m] = self.metrics[m](vectors)
        return scores

    def log_scores(self, scores: dict, name: str, trainer: Trainer):
        for m in scores:
            trainer.logger.experiment.add_scalar(
                f"{name}/{m}",
                scores[m],
                trainer.global_step,
            )

    def on_validation_epoch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAEMultiDict,
    ) -> None:
        samples = pl_module.get_input(self.batch)
        if samples.device != pl_module.device:
            samples = samples.to(pl_module.device)
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        outputs = pl_module(samples)
        vectors = outputs["sparse_vector"]
        scores = self.compute_scores(vectors)
        self.log_scores(scores, "Sparsity", trainer)

    def on_test_batch_end(
        self,
        trainer: Trainer,
        pl_module: SDM_VAEMultiDict,
        outputs: STEP_OUTPUT | None,
        batch: Any,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        for m in self.metrics:
            if self.metrics[m].device != pl_module.device:
                self.metrics[m].to(pl_module.device)
        vectors = outputs["sparse_vector"]
        scores = self.compute_scores(vectors)
        self.log_scores(scores, "Test", trainer)
