import pytorch_lightning as pl, pathlib
from argparse import ArgumentParser
from pytorch_lightning.callbacks import ModelCheckpoint, early_stopping
from pytorch_lightning.profilers import SimpleProfiler
from pytorch_lightning.loggers import TensorBoardLogger
from collections import namedtuple

ArgNamespace = namedtuple(
    "ArgNamespace",
    ["config_file", "save", "resume", "profiler", "test_only"],
)


class TrainingParser(ArgumentParser):
    """This class is used to parse the arguments for the training script."""

    def __init__(self):
        super().__init__()
        self.add_argument(
            "-c",
            "--config-file",
            type=str,
            default="config/config0.yml",
            help="Path to the config file.",
        )
        self.add_argument(
            # Save flag, false by default
            "-s",
            "--save",
            action="store_true",
            help="Save the model after training.",
        )
        self.add_argument(
            # Resume flag, false by default
            "-r",
            "--resume",
            action="store_true",
            help="Resume the training from the last checkpoint.",
        )
        self.add_argument(
            # Profiler flag, false by default
            "-p",
            "--profiler",
            action="store_true",
            help="Use the profiler to analyze the training.",
        )

        self.add_argument(
            # Test only flag, false by default
            "-t",
            "--test-only",
            action="store_true",
            help="Test the model without training.",
        )
        self.args, _ = self.parse_known_args()


class LightningTrainer(pl.Trainer):
    def __init__(self, config, model_callbacks=[], args=None, **kwargs):
        self.config = config
        self.args = args
        self.callbacks = model_callbacks
        self.profiler = None

        pl.seed_everything(self.config["training"]["seed"])

        self.init_callbacks()
        self.init_profiler()
        logger = self.init_logger()

        config_tr = self.config["training"]
        super().__init__(
            devices=config_tr["devices"],
            accelerator=config_tr["accelerator"],
            max_epochs=config_tr["epochs"],
            callbacks=self.callbacks,
            logger=logger,
            limit_train_batches=config_tr["limit_train_batches"],
            default_root_dir=config_tr["root_dir"],
            enable_checkpointing=True,
            profiler=self.profiler,
            log_every_n_steps=100,
            **kwargs,
        )

    def init_callbacks(self):
        """Initialize the callbacks for the trainer."""
        config_cb = self.config["callbacks"]
        checkpoint_callback = ModelCheckpoint(
            monitor=config_cb["monitor"],
            filename="sdm_vae-{epoch:03d}-{val_loss:.2f}",
            save_top_k=config_cb["checkpoint"]["save_top"],
            mode=config_cb["mode"],
            every_n_epochs=config_cb["checkpoint"]["every_n_epochs"],
            verbose=True,
            save_last=True,
        )
        self.callbacks.append(checkpoint_callback)

        early_stopping_callback = early_stopping.EarlyStopping(
            monitor=config_cb["monitor"],
            patience=config_cb["early_stopping"]["patience"],
            verbose=True,
            mode=config_cb["mode"],
        )
        self.callbacks.append(early_stopping_callback)

    def init_profiler(self):
        if self.args.profiler:
            self.profiler = SimpleProfiler(filename="profiler_output")

    def init_logger(self):
        # Name of the subdirectory:
        # "dataset_name-model_name-latent_size x dict_size-loss_type"
        dataset_name: str = self.config["data"]["dataset"]
        dataset_name = dataset_name[:3]

        model_name: str = self.config["model"]["name"]
        model_name = model_name.replace("_", " ").title().replace(" ", "")

        latent_size: int = self.config["network"]["latent_dim"]

        dict_size: int = self.config["network"]["dict_dim"]

        loss_type: str = self.config["model"]["loss_domain"]
        loss_type = loss_type.title()

        sub_dir_name = (
            f"{dataset_name}-{model_name}-Z{latent_size}xD{dict_size}-{loss_type}Loss"
        )

        # search the log dict for existing versions with the same name
        root = pathlib.Path(self.config["training"]["root_dir"], "lightning_logs")
        try:
            log_dirs = [x.name for x in root.iterdir()]
        except FileNotFoundError:
            log_dirs = []
        version = 0
        while f"{sub_dir_name}-{version}" in log_dirs:
            version += 1

        logger = TensorBoardLogger(
            save_dir=self.config["training"]["root_dir"],
            version=f"{sub_dir_name}-{version}",
        )

        return logger
