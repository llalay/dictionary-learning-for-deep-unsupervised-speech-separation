from .librimix import LibriMixDataModule
from .tcd_timit import TCDTimitDatamodule, TCDMixDatamodule

from .transforms import (
    AudioTrim,
    AudioSpec,
    SourceSeparationSpecTransform,
    SingleAudioSpecTransform,
)
from typing import TypedDict


class SupportedTransforms(TypedDict):
    AudioTrim: AudioTrim
    AudioSpec: AudioSpec
    SourceSeparationSpecTransform: SourceSeparationSpecTransform
    SingleAudioSpecTransform: SingleAudioSpecTransform


class SupportedDatasets(TypedDict):
    LibriMix: LibriMixDataModule
    TCDTimit: TCDTimitDatamodule
    TCDMix: TCDMixDatamodule


supported_transforms = SupportedTransforms(
    AudioTrim=AudioTrim,
    AudioSpec=AudioSpec,
    SourceSeparationSpecTransform=SourceSeparationSpecTransform,
    SingleAudioSpecTransform=SingleAudioSpecTransform,
)

supported_datasets = SupportedDatasets(
    LibriMix=LibriMixDataModule,
    TCDTimit=TCDTimitDatamodule,
    TCDMix=TCDMixDatamodule,
)
