from asteroid.data import LibriMix as MiniLibriMix
from os.path import join, exists
from torch.utils.data import Dataset, DataLoader, random_split
from pytorch_lightning import LightningDataModule
from typing import Any
import os, pandas as pd


# Dataset
class MiniLibriMixDataset(MiniLibriMix):
    def __init__(
        self,
        csv_dir,
        task="sep_clean",
        sample_rate=16000,
        n_src=2,
        segment=3,
        transform=None,
    ):
        super().__init__(
            csv_dir,
            task=task,
            sample_rate=sample_rate,
            n_src=n_src,
            segment=segment,
            return_id=False,
        )
        self.transform = transform

    def __getitem__(self, index):
        mix, sources = super().__getitem__(index)
        if self.transform is not None:
            output = self.transform(mix, sources)
        return output

    @staticmethod
    def change_names_MiniLibriMix(root_mlm: str, csv_dir: str):
        """Change the path writen in the metadata to add the relative path from the script"""
        metadata_dir = join(root_mlm, csv_dir)
        files = [join(metadata_dir, f) for f in os.listdir(metadata_dir)]
        for file in files:
            df = pd.read_csv(file)
            path_keys = [key for key in df.keys() if "path" in key]
            for key in path_keys:
                if df[key][0].split("/")[0] != "MiniLibriMix":
                    print("Already changed")
                    return
                df[key] = df[key].apply(lambda x: join(root_mlm, x))
            df.to_csv(file, index=False)


# DataModule
class MiniLibriMixDataModule(LightningDataModule):
    train_set: Dataset
    val_set: Dataset
    test_set: Dataset
    config_mlm: dict
    transform: Any
    stack_spectrograms: Any

    def __init__(self, config_data: dict, transform: Any = None):
        super().__init__()
        self.csv_dir = join(config_data["path"], "MiniLibriMix", "metadata")
        self.config_mlm = config_data["MiniLibriMix"]
        self.transform = transform
        self.batch_size = config_data["batch_size"]
        self.num_workers = config_data["num_workers"]
        self.stack_spectrograms = transform.stack_spectrograms

    def prepare_data(self):
        # Download data here
        assert exists(self.csv_dir), "Please download the MiniLibriMix dataset"

    def setup(self, stage=None):
        # Define train_set, test_set and val_set here
        if stage == "fit":
            full_train_set = MiniLibriMixDataset(
                csv_dir=self.csv_dir,
                task=self.config_mlm["task"],
                n_src=self.config_mlm["num_speakers"],
                sample_rate=self.config_mlm["sr"],
                transform=self.transform,
            )
            self.train_set, self.val_set = random_split(full_train_set, [0.9, 0.1])
        if stage == "test":
            self.test_set = self.val_set

    def train_dataloader(self):
        return DataLoader(
            self.train_set,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            pin_memory=True,
            collate_fn=self.stack_spectrograms,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_set,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            pin_memory=True,
            collate_fn=lambda x: self.stack_spectrograms(
                x, need_audio=True, need_phase=True
            ),
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_set,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            pin_memory=True,
            collate_fn=lambda x: self.stack_spectrograms(
                x, need_audio=True, need_phase=True
            ),
        )

    def demo_audio_batch(self, n_audios=1):
        # Load test_set
        self.setup("test")

        # Get 1 batch
        batch_dict = next(iter(self.test_dataloader()))
        mix_frames = batch_dict["mix_frames"]
        sources_frames = batch_dict["sources_frames"]
        num_frames = batch_dict["num_frames"]
        phases = batch_dict["phases"]
        audios = batch_dict["audios"]

        # Metadata for the audio
        input_dim = mix_frames.shape[-1]
        sr = self.config_mlm["sr"]

        # Create a Minibatch of data to be used in the logs
        num_frames = num_frames[:n_audios]
        batch_dict["num_frames"] = num_frames

        mix_frames = mix_frames[: sum(num_frames)]
        batch_dict["mix_frames"] = mix_frames

        sources_frames = sources_frames[: sum(num_frames)]
        batch_dict["sources_frames"] = sources_frames

        phases = phases[:n_audios]
        batch_dict["phases"] = phases

        audios = [x[0] for x in audios[:n_audios]]
        batch_dict["audios"] = audios

        output = {
            "audios": audios,
            "batch": batch_dict,
            "input_dim": input_dim,
            "sr": sr,
        }

        return output


# *************** Fin MiniLibriMix ****************
