import torch, torchaudio
from torch.utils.data import Dataset

from typing import Tuple, Union
from pathlib import Path

_TASKS_TO_MIXTURE = {
    "sep_clean": "mix_clean",
    "enh_single": "mix_single",
    "enh_both": "mix_both",
    "sep_noisy": "mix_both",
}


class LibriMix(Dataset):
    def __init__(
        self,
        root: Union[str, Path],
        subset: str = "train-360",
        num_speakers: int = 2,
        sample_rate: int = 8000,
        task: str = "sep_clean",
        mode: str = "min",
    ) -> None:
        super().__init__()
        self.root = Path(root) / f"Libri{num_speakers}Mix"
        self.sr = sample_rate
        tmp_dir = self.root / f"wav{self.sr//1000}k" / mode / subset
        self.mix_dir = tmp_dir / _TASKS_TO_MIXTURE[task]
        # For enhancement of both sources, the source is the clean mixture
        if task == "enh_both":
            self.src_dirs = [tmp_dir / "mix_clean"]
        else:
            self.src_dirs = [tmp_dir / f"s{i+1}" for i in range(num_speakers)]

        self.sanity_check()

        self.files = [p.name for p in self.mix_dir.iterdir() if p.suffix == ".wav"]
        self.files.sort()
        self.len = len(self.files)
        self.ids = [self.get_speakers_ids(i) for i in range(self.len)]

    def sanity_check(self) -> None:
        assert self.root.exists(), f"LibriMix root {self.root} does not exist"
        assert self.sr in [8000, 16000], f"Unsupported sample rate {self.sr}"
        assert (
            self.mix_dir.exists()
        ), f"LibriMix mixture directory {self.mix_dir} does not exist"
        for src_dir in self.src_dirs:
            assert (
                src_dir.exists()
            ), f"LibriMix source directory {src_dir} does not exist"

    def __len__(self) -> int:
        return self.len

    def get_speakers_ids(self, idx: int) -> Tuple[int, ...]:
        filename = self.files[idx]
        ids = tuple(int(x.split("-")[0]) for x in filename.split("_"))

        return ids

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        # Mixture and sources share the same filename
        filename = self.files[idx]
        mix, _ = torchaudio.load(self.mix_dir / filename)
        sources = [torchaudio.load(src_dir / filename)[0] for src_dir in self.src_dirs]

        return mix, sources


def main():
    with open("config/_test.yml") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    dataset = LibriMix(**config["data"]["LibriMix"])

    d = {}
    for t in dataset.ids:
        k1 = f"{t[0]}"
        k2 = f"{t[1]}"
        if k1 not in d:
            d[k1] = 1
        else:
            d[k1] += 1
        if k2 not in d:
            d[k2] = 1
        else:
            d[k2] += 1

    print(max(d, key=d.get), max(d.values()))
    print(min(d, key=d.get), min(d.values()))


if __name__ == "__main__":
    import yaml

    main()
