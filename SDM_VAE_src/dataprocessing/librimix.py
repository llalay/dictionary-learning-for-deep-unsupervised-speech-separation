from SDM_VAE_src.dataprocessing.datasets import LibriMix

# from torchaudio.datasets import LibriMix
from typing import Any, Callable
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning import LightningDataModule
import torch


def librimix_wrapper_transform(transform) -> Callable:
    def transform_source(mix, sources, mode):
        return transform(mix, sources, mode)

    def transform_no_source(mix, sources, mode):
        return transform(mix, mode)

    if "Separation" in transform.__class__.__name__:
        libri_transform = transform_source
    else:
        libri_transform = transform_no_source

    class LibrimixTransform(LibriMix):
        def __init__(self, *args, **kwargs):
            try:
                super().__init__(*args, **kwargs)
            except TypeError as e:
                # Older version of Pytorch do not support 'mode' arg
                print(f"{e}. 'mode' arg is deleted")
                del kwargs["mode"]
                super().__init__(*args, **kwargs)
            try:
                subset = kwargs["subset"]
                if "train" in subset:
                    self.mode = "train"
                else:
                    self.mode = "eval"
            except:
                self.mode = "train"

        # def __getitem__(self, index):
        #     sr, mix, sources = super().__getitem__(index)
        #     mix = mix[0]
        #     sources = torch.stack([s[0] for s in sources])
        #     with torch.no_grad():
        #         item = libri_transform(mix, sources, self.mode)
        #     return item

        def __getitem__(self, index):
            mix, sources = super().__getitem__(index)
            mix = mix[0]
            sources = torch.stack([s[0] for s in sources])
            with torch.no_grad():
                item = libri_transform(mix, sources, self.mode)
            return item

    return LibrimixTransform


# Datamodule
class LibriMixDataModule(LightningDataModule):
    train_set: Dataset
    val_set: Dataset
    test_set: Dataset

    def __init__(self, config_data: dict, transform: Any = None):
        super().__init__()
        self.config_lm = config_data["LibriMix"]
        self.batch_size = config_data["batch_size"]
        self.num_workers = config_data["num_workers"]
        self.transform = transform
        self.collate = transform.collate_fn

    def prepare_data(self):
        # Download data here
        pass

    def setup(self, stage=None):
        # Define train_set, test_set and val_set here
        DatasetWithTransform = librimix_wrapper_transform(self.transform)
        if stage == "fit":
            self.train_set = DatasetWithTransform(
                **self.config_lm,
            )
            config_val = {**self.config_lm}
            config_val["subset"] = "dev"
            self.val_set = DatasetWithTransform(
                **config_val,
            )
        if stage == "test":
            config_test = {**self.config_lm}
            config_test["subset"] = "test"
            self.test_set = DatasetWithTransform(
                **config_test,
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_set,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_set,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_set,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def demo_audio_batch(self, n_audios=1):
        #  Load test_set
        self.setup("test")

        # Get n_audios random items from the test set
        len_test = len(self.test_set)
        indices = torch.randperm(len_test)[:n_audios]

        # Get the batch of random items
        batch = []
        for i in indices:
            item = self.test_set[i]
            if len(item["audios"].shape) > 1:
                item["audios"] = item["audios"][0]
            batch.append(item)

        # Stack the batch
        batch_dict = self.collate(batch)
        input_dim = batch_dict["input"].shape[-1]

        return {
            "batch": batch_dict,
            "input_dim": input_dim,
            "sample_rate": self.config_lm["sample_rate"],
        }
