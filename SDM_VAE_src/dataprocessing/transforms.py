import torch.nn as nn
import torch


def sine_window(window_length: int, eps: float = 1e-3) -> torch.Tensor:
    """This function creates a sine window of length window_length.

    Input:
    ------
    window_length: int
        Length of the window
    eps: float
        Value of the sine window at the edges. Default is 1e-3

    Output:
    -------
    window: torch.Tensor
        Sine window of length window_length
    """
    time = torch.linspace(0, torch.pi, window_length)
    window = torch.sin(time) + eps
    window.requires_grad = False

    return window / (1 + eps)


class AudioTrim(nn.Module):
    """
    Trim the leading and trailing silence of the audio signal below the threshold in dB
    """

    def __init__(self, threshold: int, config_STFT: dict) -> None:
        super().__init__()
        self.trim_theshold_db = threshold
        self.win_len = config_STFT["win_length"]
        self.hop = config_STFT["hop_length"]

    def trim_index(self, signal: torch.Tensor) -> tuple[int, int]:
        """This function computes the index of the first and last non silent frame

        Input:
        ------
        signal: torch.Tensor
            Shape is (num_samples)

        Output:
        -------
        first_index: int
            Index of the first non silent frame
        last_index: int
            Index of the last non silent frame
        """
        if len(signal.shape) > 1:
            signal = signal[0]

        n_frames, _ = divmod((signal.shape[-1] - self.win_len), self.hop)

        stf_shape = (n_frames, self.win_len)

        # Create Short Time Frames. STF shape is (num_frames, win_len)
        stf = torch.zeros(stf_shape)
        for k in range(n_frames):
            start = k * self.hop
            stop = start + self.win_len
            stf[k] = signal[start:stop]

        # Compute the dB level of each frame. Shape is (num_frames)
        rms_square = torch.square(stf).mean(dim=-1)
        db_rms = 10 * torch.log10(rms_square)

        # True if the frame is not silent. Shape is (num_frames)
        non_silent_frames = (db_rms > -self.trim_theshold_db) * 1

        # Find the first and last non silent frame. Shape is (batch_size, num_channels)
        first_frame = non_silent_frames.argmax()
        last_frame = n_frames - non_silent_frames.flip(dims=(0,)).argmax()

        # Deduce indexes
        first_index = first_frame * self.hop
        last_index = last_frame * self.hop + self.win_len

        return first_index, last_index

    def forward(
        self,
        signal: torch.Tensor,
        sources: torch.Tensor = None,
    ) -> torch.Tensor:
        """
        Input:
        ------
        signal: torch.Tensor
            Shape is ([num_channels,] num_samples)
        sources: torch.Tensor [optional]
            Shape is (num_sources, [num_channels,] num_samples)

        Output:
        -------
        trimmed_signal: torch.Tensor
            Shape is ([num_channels,] num_samples)
        """

        # Compute the indexes
        first_index, last_index = self.trim_index(signal)

        # Trim the signal(s)
        trimmed_signal = signal[..., first_index:last_index]

        if sources is not None:
            trimmed_sources = sources[..., first_index:last_index]
            return trimmed_signal, trimmed_sources

        return trimmed_signal


class AudioSpec(nn.Module):
    """
    Keep original phase for reconstruction.
    """

    window: torch.Tensor

    def __init__(self, config_STFT: dict, pad: bool = False) -> None:
        super().__init__()
        self.config_stft = config_STFT.copy()
        match self.config_stft["window"]:
            case "hamming":
                window = torch.hamming_window(config_STFT["win_length"])
            case "sine":
                window = sine_window(config_STFT["win_length"])
            case _:
                window = torch.hann_window(config_STFT["win_length"])

        del self.config_stft["window"]
        self.register_buffer("window", window)
        self.pad = pad
        self.n_pad = self.config_stft["n_fft"] // 2

    def forward(
        self, signal: torch.Tensor, keep_phase: bool = False
    ) -> dict[str, torch.Tensor]:
        if self.pad:
            s = signal.shape
            padded_shape = (*s[:-1], s[-1] + 2 * self.n_pad)
            signal = torch.nn.functional.pad(
                signal.view(1, *signal.shape),
                (self.n_pad, self.n_pad),
                mode="reflect",
            ).view(padded_shape)
        stft = torch.stft(
            signal,
            **self.config_stft,
            window=self.window,
            return_complex=True,
        )

        spec = torch.square(torch.abs(stft))
        output = {"spec": spec}

        if keep_phase:
            phase = torch.angle(stft)
            output["phase"] = phase

        return output

    def inverse(self, spec: torch.Tensor, phase: torch.Tensor) -> torch.Tensor:
        stft = torch.sqrt(spec) * torch.exp(1j * phase)
        signal = torch.istft(
            stft,
            **self.config_stft,
            window=self.window.to(spec),
            return_complex=False,
        )

        if self.pad:
            signal = signal[..., self.n_pad : -self.n_pad]

        return signal


class SourceSeparationSpecTransform(nn.Module):
    reconstruction: bool = False
    sources: bool = False

    def __init__(self, config_transform: dict) -> None:
        super().__init__()
        config_STFT = config_transform["STFT"]
        threshold = config_transform["trim_threshold"]
        self.trim = AudioTrim(threshold, config_STFT)
        self.spec = AudioSpec(config_STFT, pad=True)

    def mode_setup(self, mode: str):
        match mode:
            case "train":
                self.reconstruction = True
            case "eval":
                self.reconstruction = True
            case _:
                raise ValueError(f"Unknown mode {mode}")

    def forward(
        self,
        mix: torch.Tensor,
        sources: torch.Tensor,
        speakers: list[str] = None,
        mode: str = "train",
    ) -> dict[str, torch.Tensor]:
        """
        Computes the spectrogram of the mix and the sources.
        """
        output = {}

        self.mode_setup(mode)

        # Normalize the mix
        mix /= mix.abs().max(dim=-1, keepdim=True).values

        trimmed_mix, trimmed_sources = self.trim(mix, sources)
        spec_mix_dict = self.spec(trimmed_mix, keep_phase=self.reconstruction)
        spec_sources_dict = self.spec(trimmed_sources, keep_phase=False)

        output["spec_mix"] = spec_mix_dict["spec"]
        output["spec_sources"] = spec_sources_dict["spec"]
        output["speakers"] = speakers

        if self.reconstruction:
            output["phase_mix"] = spec_mix_dict["phase"]
            output["audios"] = trimmed_mix
            output["sources_audios"] = trimmed_sources

        return output

    def inverse(self, spec_mix: torch.Tensor, phase_mix: torch.Tensor) -> torch.Tensor:
        return self.spec.inverse(spec_mix, phase_mix)

    @staticmethod
    def collate_fn(batch: list[dict[str, torch.Tensor]]) -> dict[str, torch.Tensor]:
        """Function to feed the dataloader."""
        stacked_spec_mix = []
        stacked_spec_sources = []
        speakers = []
        num_frames = []

        for item in batch:
            spec_mix = item["spec_mix"]
            spec_sources = item["spec_sources"]
            n_frames = spec_mix.shape[-1]

            stacked_spec_mix.append(spec_mix.T)
            stacked_spec_sources.append(torch.transpose(spec_sources, 1, 2))
            speakers.append(item["speakers"])
            num_frames.append(n_frames)

        stacked_spec_mix = torch.cat(stacked_spec_mix)
        stacked_spec_sources = torch.cat(stacked_spec_sources, dim=1)

        output = {
            "input": stacked_spec_mix,
            "ground_truth": stacked_spec_sources,
            "num_frames": torch.tensor(num_frames),
            "speakers": speakers,
        }

        # Check if we need to reconstruct the audio
        if len(batch[0].keys()) > 2:
            audios = []
            sources_audios = []
            phases = []
            for item in batch:
                audios.append(item["audios"])
                sources_audios.append(item["sources_audios"])
                phases.append(item["phase_mix"])
            output["audios"] = audios
            output["sources_audios"] = sources_audios
            output["phases"] = phases

        return output

    @staticmethod
    def unstack(
        frames: torch.Tensor,
        num_frames: torch.Tensor,
    ):
        unstacked_frames = []
        start = 0
        for n in num_frames:
            unstacked_frames.append(
                torch.transpose(frames[..., start : start + n, :], -2, -1)
            )
            start += n
        return unstacked_frames


class SingleAudioSpecTransform(nn.Module):
    reconstruction: bool = False

    def __init__(self, config_transform: dict) -> None:
        super().__init__()
        self.config_t = config_transform
        config_STFT = config_transform["STFT"]
        threshold = config_transform["trim_threshold"]
        self.trim = AudioTrim(threshold, config_STFT)
        self.spec = AudioSpec(config_STFT, pad=True)

    def mode_setup(self, mode: str):
        match mode:
            case "train":
                self.reconstruction = self.config_t["train_need_reconstruct"]
            case "eval":
                self.reconstruction = True
            case _:
                raise ValueError(f"Unknown mode {mode}")

    def forward(self, audio: torch.Tensor, mode: str) -> dict[str, torch.Tensor]:
        output = {}

        self.mode_setup(mode)

        audio /= audio.abs().max(dim=-1, keepdim=True).values

        trimmed_audio = self.trim(audio)
        spec_dict = self.spec(trimmed_audio, keep_phase=self.reconstruction)

        output["spec"] = spec_dict["spec"]

        if self.reconstruction:
            output["phase"] = spec_dict["phase"]
            output["audios"] = trimmed_audio

        return output

    def inverse(self, spec: torch.Tensor, phase: torch.Tensor) -> torch.Tensor:
        return self.spec.inverse(spec, phase)

    @staticmethod
    def collate_fn(batch: list[dict[str, torch.Tensor]]) -> dict[str, torch.Tensor]:
        stacked_spec = []
        num_frames = []

        for item in batch:
            spec = item["spec"]
            n_frames = spec.shape[-1]

            stacked_spec.append(spec.T)
            num_frames.append(n_frames)

        stacked_spec = torch.cat(stacked_spec)

        output = {
            "input": stacked_spec,
            "num_frames": torch.tensor(num_frames),
        }
        if "speaker" in batch[0].keys():
            speakers = [item["speaker"] for item in batch]
            output["speakers"] = speakers

        if len(batch[0].keys()) > 1:
            audios = []
            phases = []
            for item in batch:
                audios.append(item["audios"])
                phases.append(item["phase"])
            output["audios"] = audios
            output["phases"] = phases

        return output

    @staticmethod
    def unstack(
        frames: torch.Tensor,
        num_frames: torch.Tensor,
    ):
        unstacked_frames = []
        start = 0
        for n in num_frames:
            unstacked_frames.append(frames[start : start + n].T)
            start += n
        return unstacked_frames
