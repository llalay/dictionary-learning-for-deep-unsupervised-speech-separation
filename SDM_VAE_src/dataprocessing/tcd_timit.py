from torch.utils.data import Dataset, DataLoader
from pathlib import Path
from os import listdir
from typing import Any
from pytorch_lightning import LightningDataModule
import torch, torchaudio
from config import ConfigClass
from .transforms import SourceSeparationSpecTransform


class TCDTimit(Dataset):
    def __init__(self, root_dir, sr, subset: str = "train"):
        super().__init__()
        self.root_dir = Path(root_dir)
        self.subset = subset
        self.files, self.speakers, self.tot_num_files = self.get_audio_filenames(subset)
        if subset == "train":
            # Add test set to train set because this is supervised
            f, s, t_n = self.get_audio_filenames("test")
            self.files += f
            self.speakers += s
            self.tot_num_files += t_n

        # Get sample rate
        self.sample_rate = sr
        original_sr = torchaudio.info(self.files[0][0]).sample_rate

        if original_sr != sr:
            self.resample = torchaudio.transforms.Resample(
                orig_freq=original_sr, new_freq=sr
            )
        else:
            self.resample = lambda x: x

    def get_audio_filenames(self, subset):
        # Directory name
        if subset not in ["train", "test", "val"]:
            raise ValueError("Mode must be either train, test or val")
        dir = self.root_dir / f"{subset}_data"
        if subset == "test":
            dir /= "clean"

        # List of speakers
        speakers = listdir(dir)

        # For all speakers, get the list of files
        files = []
        tot_num_files = 0
        for s in speakers:
            speaker_files = [dir / s / f for f in listdir(dir / s) if f.endswith("wav")]
            n = len(speaker_files)
            for i in range(n):
                files.append((speaker_files[i], s))
            tot_num_files += n
        if subset == "test":
            # Due to a large power spike in the audio, this file is removed
            spike_file = self.root_dir / "test_data/clean/33F/sx125.wav"
            files.remove((spike_file, "33F"))
            tot_num_files -= 1

        return files, speakers, tot_num_files

    def __len__(self):
        return self.tot_num_files

    def __getitem__(self, index):
        filename, speaker = self.files[index]
        audio, _ = torchaudio.load(filename)
        audio = self.resample(audio)

        return audio.view(-1), speaker


def tcd_transform_wrapper(transform):
    """
    Here define everything specific to either the dataset or the transform forward call
    """

    class TCDTimitTransform(TCDTimit):
        def __getitem__(self, index):
            audio, speaker = super().__getitem__(index)
            if self.subset in ["test", "val"]:
                mode = "eval"
            else:
                mode = "train"
            with torch.no_grad():
                item = transform(audio, mode=mode)
                item["speaker"] = speaker
            # audio_len = len(audio)
            # trim_len = item["audios"].shape[0]
            # trim_ratio = trim_len / audio_len
            # if trim_ratio < 0.2:
            #     spec_shape = item["spec"].shape
            #     filename = self.files[index][0]
            #     text = f"\n\n\n\nSpeaker: {speaker}, Index: {index}\n"
            #     text += f"Filename: {filename}\n"
            #     text += f"Audio length: {audio_len}\n"
            #     text += f"Trimmed length: {trim_len}\n"
            #     text += f"Trimmed ratio: {trim_ratio}\n"
            #     text += f"Spec shape: {spec_shape}\n"
            #     print(text)
            return item

    return TCDTimitTransform


class TCDTimitDatamodule(LightningDataModule):
    def __init__(self, config_data: dict, transform: Any = None):
        super().__init__()
        self.config_data = config_data
        self.batch_size = config_data["batch_size"]
        self.num_workers = config_data["num_workers"]
        self.transform = transform
        self.collate = self.transform.collate_fn

    def setup(self, stage: str) -> None:
        DatasetWithTransform = tcd_transform_wrapper(self.transform)
        if stage == "fit":
            self.train_set = DatasetWithTransform(
                **self.config_data["TCDTimit"], subset="train"
            )
            self.val_set = DatasetWithTransform(
                **self.config_data["TCDTimit"], subset="val"
            )
        if stage == "test":
            self.test_set = DatasetWithTransform(
                **self.config_data["TCDTimit"], subset="test"
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_set,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_set,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_set,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def demo_audio_batch(self, n_audios=1):
        #  Load test_set
        self.setup("test")
        self.setup("fit")

        # Get n_audios random items from the test set
        len_test = len(self.train_set)
        indices = torch.randperm(len_test)[:n_audios]

        # Get the batch of random items
        batch = [self.train_set[i] for i in indices]

        # Stack the batch
        batch_dict = self.collate(batch)
        # Disable formatter when adding lists
        # fmt: off
        batch_dict["all_speakers"] = self.train_set.speakers + self.val_set.speakers + self.test_set.speakers
        # fmt: on

        input_dim = batch_dict["input"].shape[-1]

        return {
            "batch": batch_dict,
            "input_dim": input_dim,
            "sample_rate": self.test_set.sample_rate,
        }


class TCDMix(Dataset):
    """Mix version of TCD Timit. Each utterance is present twice in the dataset,
    mixed with another utterance from another speaker."""

    def __init__(
        self,
        root_dir,
        sr,
        subset: str = "train",
        transform: SourceSeparationSpecTransform = None,
        n_mix: int = 2,
        mode: str = "max",
    ):
        super().__init__()
        self.root_dir = Path(root_dir)
        self.subset = subset
        self.transform = transform
        self.n_mix = n_mix
        self.mode = mode

        train_files, train_speakers = self.get_audio_filenames("train")
        val_files, val_speakers = self.get_audio_filenames("val")
        test_files, test_speakers = self.get_audio_filenames("test")

        self.speakers = train_speakers + val_speakers + test_speakers
        all_files = train_files + val_files + test_files

        train_files = []
        val_files = []
        test_files = []

        # In supervised mode all speakers must be present in all the sets
        for files, speaker in all_files:
            len_train = round(len(files) * 0.8)
            len_val = round(len(files) * 0.1)
            len_test = len(files) - len_train - len_val

            train_files.append((files[:len_train], speaker))
            val_files.append((files[len_train : len_train + len_val], speaker))
            test_files.append((files[-len_test:], speaker))

        match subset:
            # Proportion may not be exact due to rounding
            case "train":
                # 80%
                self.get_mix_files(train_files)
            case "val":
                # 10%
                self.get_mix_files(val_files)
            case "test":
                # 10%
                self.get_mix_files(test_files)

        self.tot_num_files = len(self.files)

        # Get sample rate
        self.sample_rate = sr
        original_sr = torchaudio.info(test_files[0][0][0]).sample_rate

        if original_sr != sr:
            self.resample = torchaudio.transforms.Resample(
                orig_freq=original_sr, new_freq=sr
            )
        else:
            self.resample = lambda x: x

        self.prepare_data()

    def get_audio_filenames(self, subset):
        """Retrieve all the audio files in the subset. Group them by speaker."""

        # Directory name
        if subset not in ["train", "test", "val"]:
            raise ValueError("Mode must be either train, test or val")
        dir = self.root_dir / f"{subset}_data"
        if subset == "test":
            dir /= "clean"

        # List of speakers
        speakers = listdir(dir)

        # For all speakers, get the list of files
        files = []
        for s in speakers:
            speaker_files = [dir / s / f for f in listdir(dir / s) if f.endswith("wav")]
            files.append((speaker_files, s))

        return files, speakers

    def get_mix_files(self, files):
        """Mix all the files in the subset. Each utterance is present at least once.
        All the speakers are present in the validation and test sets."""

        # The 98 sentences are common to all speakers. A mix must contain two different sentences.
        # All the files must be used at least once.

        # files[speaker_idex][0:filenames, 1:speaker_name][sentence_index]

        mix_files = []
        n_speakers = len(self.speakers)

        for i in range(n_speakers):
            # Detect the number of sentences for the current speaker
            n_sentences = len(files[i][0])

            # For each sentence of the current speaker, mix with the next sentence of the next speaker
            for j in range(n_sentences):
                item = {"files": [], "speakers": []}
                for k in range(self.n_mix):
                    # i => speaker, j => sentence, k => mix_index

                    # We want to keep the speaker ids so each item is
                    # {"files": [file1, file2, ...], "speakers": [speaker1, speaker2, ...]}

                    # Mix sentence j of speaker i with sentence j+k of speaker i+k to avoid
                    # mixing the same sentence from different speakers

                    speaker_idx = (i + k) % n_speakers
                    sentence_idx = (j + k) % len(files[speaker_idx][0])
                    filename = files[speaker_idx][0][sentence_idx]

                    item["files"].append(filename)
                    item["speakers"].append(files[speaker_idx][1])
                mix_files.append(item)

        self.files = mix_files

    def mix_signals(self, item):
        """Mix the signals from the files in item["files"]"""

        # Load the files
        signals = []
        for filename in item["files"]:
            signal, _ = torchaudio.load(filename)
            signal = self.resample(signal)
            signals.append(signal.view(-1))

        # Pad the signals to the same length
        if self.mode == "max":
            # Add zeros to the shortest signals
            signal_len = max([s.shape[0] for s in signals])
        else:
            # Trim the longest signals
            signal_len = min([s.shape[0] for s in signals])

        pad_signals = torch.zeros(len(signals), signal_len)
        for i, signal in enumerate(signals):
            pad_signals[i] = torch.nn.functional.pad(
                signal, (0, signal_len - signal.shape[0])
            )

        # Mix the signals
        mixed_signal = torch.sum(pad_signals, dim=0)

        return mixed_signal, pad_signals

    @staticmethod
    def get_speakers(root_dir):
        speakers = []
        for subset in ["train", "test", "val"]:
            dir = Path(root_dir) / f"{subset}_data"
            if subset == "test":
                dir /= "clean"

            # List of speakers
            speakers += listdir(dir)
        return speakers

    def prepare_data(self):
        """Compute all the mixes for the subset and store it"""

        # Create the directory if it does not exist
        mix_dir = self.root_dir.parent
        mix_dir = mix_dir / "TCDMix" / f"{self.n_mix}_mix"
        mix_dir = mix_dir / f"{self.mode}" / f"{self.sample_rate}Hz"
        mix_dir = mix_dir / f"{self.subset}"
        mix_dir.mkdir(parents=True, exist_ok=True)
        self.mix_dir = mix_dir

        if len(listdir(mix_dir)) == self.tot_num_files * 3:
            # All the files are already mixed
            return

        for i, filenames in enumerate(self.files):
            mix, sources = self.mix_signals(filenames)
            torchaudio.save(mix_dir / f"{i}_mix.wav", mix.view(1, -1), self.sample_rate)
            for j, s in enumerate(sources):
                torchaudio.save(
                    mix_dir / f"{i}_s{j}.wav", s.view(1, -1), self.sample_rate
                )

    def load_data(self, index):
        mix = torchaudio.load(self.mix_dir / f"{index}_mix.wav")[0].view(-1)
        sources = torch.stack(
            [
                torchaudio.load(self.mix_dir / f"{index}_s{j}.wav")[0].view(-1)
                for j in range(self.n_mix)
            ]
        )
        return mix, sources

    def __len__(self):
        return self.tot_num_files

    def __getitem__(self, index):
        filenames = self.files[index]
        mix, sources = self.load_data(index)
        speakers = filenames["speakers"]

        if self.transform is not None:
            return self.transform(mix, sources, speakers)

        return mix, sources, speakers


class TCDMixDatamodule(LightningDataModule):
    speakers: list[str]

    def __init__(self, config: ConfigClass, transform: Any = None):
        super().__init__()
        config_data = config["data"]
        self.batch_size = config_data["batch_size"]
        self.num_workers = config_data["num_workers"]
        self.root_dir = config_data["TCDTimit"]["root_dir"]
        self.sr = config["global sr"]
        self.transform = transform
        self.collate = self.transform.collate_fn
        self.speakers = TCDMix.get_speakers(self.root_dir)

    def setup(self, stage: str) -> None:
        self.train_set = TCDMix(
            root_dir=self.root_dir,
            sr=self.sr,
            subset="train",
            transform=self.transform,
        )

        self.val_set = TCDMix(
            root_dir=self.root_dir,
            sr=self.sr,
            subset="val",
            transform=self.transform,
        )

        self.test_set = TCDMix(
            root_dir=self.root_dir,
            sr=self.sr,
            subset="test",
            transform=self.transform,
        )

    def train_dataloader(self):
        return DataLoader(
            self.train_set,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_set,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_set,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.collate,
        )


def main():
    root = Path("../../Datasets/TCD-TIMIT")
    sr = 16000

    tik = time()
    test_set = TCDMix(root, sr, subset="test")
    tok = time()
    print(f"Test set loaded in {tok-tik:.2f} seconds")

    tik = time()
    train_set = TCDMix(root, sr, subset="train")
    tok = time()
    print(f"Train set loaded in {tok-tik:.2f} seconds")

    tik = time()
    val_set = TCDMix(root, sr, subset="val")
    tok = time()
    print(f"Val set loaded in {tok-tik:.2f} seconds")


if __name__ == "__main__":
    from time import time

    main()
