from .sparse_metrics import HoyerSparsity
from torchmetrics import (
    ScaleInvariantSignalDistortionRatio as SI_SDR,
    ScaleInvariantSignalNoiseRatio as SI_SNR,
)
from torchmetrics.audio import (
    PerceptualEvaluationSpeechQuality as PESQ,
    ShortTimeObjectiveIntelligibility as STOI,
)

supported_audio_metrics = {
    "si_sdr": SI_SDR,
    "si_snr": SI_SNR,
    "pesq": PESQ,
    "stoi": STOI,
}

supported_sparse_metrics = {
    "hoyer": HoyerSparsity,
}
