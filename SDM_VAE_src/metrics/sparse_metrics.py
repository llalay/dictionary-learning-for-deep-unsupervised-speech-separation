from torchmetrics.metric import Metric
from typing import Any, Callable, Optional
from torch import Tensor, norm, tensor


class HoyerSparsity(Metric):
    is_differentiable = True
    higher_is_better = True
    sum_hoyer_index: Tensor
    total: Tensor

    def __init__(
        self,
        compute_on_step: bool = True,
        dist_sync_on_step: bool = False,
        process_group: Optional[Any] = None,
        dist_sync_fn: Optional[Callable[[Tensor], Tensor]] = None,
    ):
        super().__init__(
            compute_on_step=compute_on_step,
            dist_sync_on_step=dist_sync_on_step,
            process_group=process_group,
            dist_sync_fn=dist_sync_fn,
        )
        self.add_state("sum_hoyer_index", default=tensor(0.0), dist_reduce_fx="sum")
        self.add_state("total", default=tensor(0), dist_reduce_fx="sum")

    def update(self, batch, *args, **kwargs):
        """Update Hoyer index
        Args:
            batch: Vectors
        """
        hoyer_index_batch = hoyer_sparsity(batch)

        self.sum_hoyer_index += hoyer_index_batch.sum()
        self.total += hoyer_index_batch.numel()
    
    def compute(self) -> Tensor:
        """Computes average Hoyer index."""
        return self.sum_hoyer_index / self.total

def hoyer_sparsity(batch: Tensor)-> Tensor:
    """Compute the Hoyer projection, according to:

    Hoyer, Patrik O. "Non-negative matrix factorization with sparseness constraints." Journal of machine learning research 5.9 (2004).
    Range from 0 to 1, 0 being the most dense and 1 being the most sparse.

    Input:
        vector: shape = [....time]
    Output:
        hoyer_index: float
    """

    norm_1 = norm(input=batch, p=1, dim=-1)
    norm_2 = norm(input=batch, p=2, dim=-1)
    sqrt_length = batch.shape[-1]**0.5

    hoyer_index = (sqrt_length - norm_1 / norm_2) / (sqrt_length - 1)

    return hoyer_index

def main():
    dummy_batch = torch.rand(64, 24000)
    hoyer = HoyerSparsity()
    print(hoyer(dummy_batch))

if __name__ == "__main__":
    import torch
    main()