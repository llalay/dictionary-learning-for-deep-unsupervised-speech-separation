from . import models
import yaml, torch, matplotlib.pyplot as plt


def test_vae():
    with open("config/config0.yml") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    config["network"]["input_dim"] = 513
    model = models.sdm_vae.SDM_VAE(config)
    batch_size = 100
    latent_dim = 512
    dict_size = 64
    dummy_input = torch.rand(batch_size, 513)
    out = model(dummy_input)
    batch_idx = 10
    non_zero_idx = 2

    dct = models.utils.create_dictionary_dct(latent_dim, dict_size)
    activation = torch.zeros(batch_size, dict_size)
    activation[batch_idx, non_zero_idx] = 1
    latent_vector = activation @ dct.T

    assert torch.allclose(
        latent_vector[batch_idx, :],
        dct[:, non_zero_idx],
    ), "Wrong matrix multiplication"
