from . import dataprocessing
import torch, yaml, matplotlib.pyplot as plt
from torchaudio.datasets import LibriMix
from torch.utils.data import Dataset, DataLoader, random_split
from typing import Any
from pytorch_lightning import LightningDataModule
import torch.nn as nn


class LibriMixDataset(LibriMix):
    def __init__(
        self,
        root,
        subset: str = "train-360",
        num_speakers: int = 2,
        sample_rate: int = 8000,
        task: str = "sep_clean",
        mode: str = "min",
        transform=None,
    ):
        super().__init__(
            root=root,
            subset=subset,
            num_speakers=num_speakers,
            sample_rate=sample_rate,
            task=task,
            # mode=mode,
        )
        self.transform = transform

    def __getitem__(self, index):
        sr, mix, sources = super().__getitem__(index)
        # shapes are mix: (n_channels, n_samples), sources: (n_speakers, n_channels, n_samples)
        # sources is a list of tensors

        # First reshape everything to (n_samples) since this is single channel
        mix = mix[0]
        sources = [s[0] for s in sources]

        if self.transform is not None:
            return self.transform(mix, torch.stack(sources))
        return mix, sources


class OldLibriMixDataModule(LightningDataModule):
    train_set: Dataset
    val_set: Dataset
    test_set: Dataset
    config_mlm: dict
    transform: Any
    stack_spectrograms: Any

    def __init__(self, config_data: dict, transform: Any = None):
        super().__init__()
        self.config_lm = config_data["LibriMix"]
        self.batch_size = config_data["batch_size"]
        self.num_workers = config_data["num_workers"]
        self.transform = transform
        if transform is not None:
            self.stack_spectrograms = transform.stack_spectrograms

    def prepare_data(self):
        # Download data here
        pass

    def setup(self, stage=None):
        # Define train_set, test_set and val_set here
        if stage == "fit":
            self.train_set = LibriMixDataset(
                **self.config_lm,
                transform=self.transform,
            )
            config_val = {k: self.config_lm[k] for k in self.config_lm if k != "subset"}
            config_val["subset"] = "dev"
            self.val_set = LibriMixDataset(
                **config_val,
                transform=self.transform,
            )
        if stage == "test":
            config_test = {
                k: self.config_lm[k] for k in self.config_lm if k != "subset"
            }
            config_test["subset"] = "test"
            self.test_set = LibriMixDataset(
                **config_test,
                transform=self.transform,
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_set,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=self.stack_spectrograms,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_set,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=lambda x: self.stack_spectrograms(
                x, need_audio=True, need_phase=True
            ),
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_set,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=self.num_workers,
            persistent_workers=True,
            collate_fn=lambda x: self.stack_spectrograms(
                x, need_audio=True, need_phase=True
            ),
        )

    def demo_audio_batch(self, n_audios=1):
        #  Load test_set
        self.setup("test")

        # Get n_audios random items from the test set
        len_test = len(self.test_set)
        indices = torch.randperm(len_test)[:n_audios]

        # Get the batch of random items
        batch = []
        for i in indices:
            item = self.test_set[i]
            item["audio_signals"] = item["audio_signals"][0]
            batch.append(item)
        input_dim = item["spec_mix"].shape[-1]

        # Stack the batch
        batch_dict = self.stack_spectrograms(batch, need_audio=True, need_phase=True)

        return {
            "batch": batch_dict,
            "input_dim": input_dim,
            "sample_rate": self.config_lm["sample_rate"],
        }


def sine_window(window_length: int, eps: float = 1e-3) -> torch.Tensor:
    """This function creates a sine window of length window_length.

    Input:
    ------
    window_length: int
        Length of the window
    eps: float
        Value of the sine window at the edges. Default is 1e-3

    Output:
    -------
    window: torch.Tensor
        Sine window of length window_length
    """
    time = torch.linspace(0, torch.pi, window_length)
    window = torch.sin(time) + eps
    window.requires_grad = False

    return window / (1 + eps)


class SpeechSpectroTransform(nn.Module):
    """This class is used to create Spectrograms from waveforms. It includes normalization,
    trim and padding. The transform is called in the __getitem__ method of the dataset.
    """

    def __init__(self, config_transform):
        super().__init__()
        self.config_stft = config_transform["STFT"].copy()
        self.win_len = config_transform["STFT"]["win_length"]
        match self.config_stft["window"]:
            case "hamming":
                window = torch.hamming_window(self.win_len)
            case "sine":
                window = sine_window(self.win_len)
            case _:
                window = torch.hann_window(self.win_len)

        del self.config_stft["window"]
        self.register_buffer("window", window)
        self.hop = config_transform["STFT"]["hop_length"]
        self.nfft = config_transform["STFT"]["n_fft"]
        self.trim_theshold_db = config_transform["trim_threshold"]

    def normalize(self, x):
        return x / torch.max(torch.abs(x))

    def trim_index(self, mix):
        """This function removes leading and ending silence from the signal. Only computed for
        the mix. The same indexes are then used to trim the sources.

        Input:
        ------
        mix: torch.Tensor
            Shape is (num_samples)

        Output:
        -------
        first_index: int
            Index of the first non silent frame
        last_index: int
            Index of the last non silent frame
        """
        n_frames, _ = divmod((mix.shape[0] - self.win_len), self.hop)

        # if remainder:
        #     # If the last frame is not full, we pad it with its mean
        #     n_frames += 1
        #     tmp = torch.zeros(n_frames * self.hop + self.win_len)
        #     tmp[: mix.shape[0]] = mix
        #     tmp[mix.shape[0] :] = mix[-remainder:].mean()
        #     mix = tmp

        stf_shape = (n_frames, self.win_len)

        # Create Short Time Frames. STF shape is (num_frames, win_len)
        stf = torch.zeros(stf_shape)
        for k in range(n_frames):
            start = k * self.hop
            stop = start + self.win_len
            stf[k] = mix[start:stop]

        # Compute the dB level of each frame. Shape is (num_frames)
        rms_square = torch.square(stf).mean(dim=-1)
        db_rms = 10 * torch.log10(rms_square)

        # True if the frame is not silent. Shape is (num_frames)
        non_silent_frames = (db_rms > -self.trim_theshold_db) * 1

        # Find the first and last non silent frame. Shape is (batch_size, num_channels)
        first_frame = non_silent_frames.argmax()
        last_frame = n_frames - non_silent_frames.flip(dims=(0,)).argmax()

        # Deduce indexes
        first_index = first_frame * self.hop
        last_index = last_frame * self.hop + self.win_len

        return first_index, last_index

    def trim_pad(self, mix, sources):
        """Trim and pad the signals"""
        first_index, last_index = self.trim_index(mix)

        # Trim all the signals
        mix = mix[first_index:last_index]
        sources = sources[:, first_index:last_index]

        # Pad all the signals at once, torch only implemented pad for non 1D tensors
        signals = torch.cat([mix.resize_(1, mix.shape[0]), sources], dim=0)
        n_pad = (self.nfft // 2, self.nfft // 2)

        # Pad the signals by reflecting themselves with n_fft // 2 samples on each side
        signals = torch.nn.functional.pad(signals, n_pad, mode="reflect")

        return signals

    def spec(self, signals):
        """
        Input:
        ------
        signals: torch.Tensor (num_signals, num_samples)
            signals[0] is the mixture
            signals[1:] are the sources

        Output:
        -------
        spec: torch.Tensor
            (num_signals, n_bins, num_frames)
        phases: torch.Tensor
            (num_signals, n_bins, num_frames)
        """
        # Compute STFT. Output is complex. Shape is (num_signals, n_bins, num_frames)
        stft = torch.stft(
            signals,
            **self.config_stft,
            window=self.window,
            return_complex=True,
        )

        # Extract the phase for reconstruction.
        abs_stft = torch.abs(stft)
        phase_mix = torch.angle(stft[0])

        # Compute the power spectrogram. Shape is (num_signals, n_bins, num_frames)
        power_spec = torch.square(abs_stft)

        return power_spec, phase_mix

    def forward(self, mix, sources):
        """
        Transform the waveforms into spectrograms
        Input:
        ------
        mix: torch.Tensor
            (num_samples)
        sources: torch.Tensor
            (num_sources, num_samples)

        Output:
        -------
        spec_mix: torch.Tensor
            (n_bins, num_frames)
        spec_sources: torch.Tensor
            (num_sources, n_bins, num_frames)
        phase_mix: torch.Tensor
            (n_bins, num_frames)
        audio_signals: torch.Tensor
            (num_signals, num_samples)
        """
        mix = self.normalize(mix)
        signals = self.trim_pad(mix, sources)
        power_spec, phase_mix = self.spec(signals)

        # Un-pad signals
        signals = signals[:, self.nfft // 2 : -self.nfft // 2]

        spec_mix = power_spec[0]
        spec_sources = power_spec[1:]

        output = {
            "spec_mix": torch.transpose(spec_mix, 0, 1),
            "spec_sources": torch.transpose(spec_sources, 1, 2),
            "phase_mix": phase_mix,
            "audio_signals": signals,
        }

        return output

    def backward(self, spec_mix, phase_mix):
        """
        Transform the spectrogram back into waveform

        Input:
        ------
        spec_mix: torch.Tensor
            (n_bins, num_frames)
        phase_mix: torch.Tensor
            (n_bins, num_frames)

        Output:
        -------
        mix: torch.Tensor (num_samples)
        """
        # Reconstruct the STFT
        stft = torch.sqrt(spec_mix.T) * torch.exp(1j * phase_mix)

        # Reconstruct the signal
        signal = torch.istft(
            stft,
            **self.config_stft,
            window=self.window.to(stft.device),
            return_complex=False,
        )

        # Remove the padding
        signal = signal[self.nfft // 2 : -self.nfft // 2]

        return signal

    @staticmethod
    def stack_spectrograms(batch, need_audio=False, need_phase=False):
        """
        Stack the spectrograms of the batch to build one big spectrogram with
        shape (frames, n_bins)
        """

        n_fft = batch[0]["spec_mix"].shape[1]
        n_sources = batch[0]["spec_sources"].shape[0]

        # Count the total number of frames and stack phases and audio
        num_frames = []
        phases = []
        audios = []

        tot_num_frames = 0
        for dict in batch:
            n_frame_i = len(dict["spec_mix"])
            tot_num_frames += n_frame_i
            num_frames.append(n_frame_i)

            if need_phase:
                phase_mix = dict["phase_mix"]
                phases.append(phase_mix)

            if need_audio:
                audio = dict["audio_signals"]
                audios.append(audio)

        # Create the big spectrogram
        mix_frames = torch.zeros(tot_num_frames, n_fft)
        sources_frames = torch.zeros(n_sources, tot_num_frames, n_fft)

        # Stack the spectrograms
        start_index, end_index = 0, 0
        for i, dict in enumerate(batch):
            mix = dict["spec_mix"]
            sources = dict["spec_sources"]

            end_index += num_frames[i]
            mix_frames[start_index:end_index] = mix
            sources_frames[:, start_index:end_index] = sources
            start_index = end_index

        output = {
            "mix_frames": mix_frames,
            "sources_frames": sources_frames,
            "num_frames": num_frames,
            "phases": phases,
            "audios": audios,
        }
        return output

    @staticmethod
    def unstack_spectrograms(mix_frames, num_frames):
        """
        Unstack the big spectrogram to retrieve the original batch

        Input:
        ------
        mix_frames: torch.Tensor
            (frames, n_bins)
        num_frames: list
            List of number of frames for each spectrogram of the batch

        Output:
        -------
        mix_spectrograms: list
            (batch_size, variable_num_frames, n_bins)
        """

        mix_spectrograms = []

        start_index, end_index = 0, 0
        for n_frames in num_frames:
            end_index += n_frames

            mix_spectrograms.append(mix_frames[start_index:end_index, :])

            start_index = end_index

        return mix_spectrograms


def _test_ss_spec_transform(config):
    config_t = config["transform"]

    old_transform = SpeechSpectroTransform(config_t)
    new_transform = dataprocessing.SourceSeparationSpecTransform(config_t)
    new_single_transform = dataprocessing.SingleAudioSpecTransform(config_t)

    dm = dataprocessing.LibriMixDataModule(config["data"], new_transform)
    single_dm = dataprocessing.LibriMixDataModule(config["data"], new_single_transform)
    old_dm = OldLibriMixDataModule(config["data"], old_transform)

    dm.setup("test")
    single_dm.setup("test")
    old_dm.setup("test")

    for n, (batch, single_batch, old_batch) in enumerate(
        zip(dm.test_dataloader(), single_dm.test_dataloader(), old_dm.test_dataloader())
    ):
        assert torch.allclose(
            batch["input"], old_batch["mix_frames"]
        ), "mix_frames not equal"
        assert torch.allclose(
            single_batch["input"], old_batch["mix_frames"]
        ), "mix_frames not equal"

        assert torch.allclose(
            batch["ground_truth"], old_batch["sources_frames"]
        ), "sources_frames not equal"

        for phi_0, phi_1 in zip(batch["phases"], old_batch["phases"]):
            assert torch.allclose(phi_0, phi_1), "phases not equal"
        for phi_0, phi_1 in zip(single_batch["phases"], old_batch["phases"]):
            assert torch.allclose(phi_0, phi_1), "phases not equal"

        assert torch.allclose(
            batch["num_frames"], torch.tensor(old_batch["num_frames"])
        ), "num_frames not equal"
        assert torch.allclose(
            single_batch["num_frames"], torch.tensor(old_batch["num_frames"])
        ), "num_frames not equal"

        for x0, x1 in zip(batch["audios"], old_batch["audios"]):
            assert torch.allclose(x0, x1), "audios not equal"
        if n > 100:
            break

    dm.setup("fit")
    single_dm.setup("fit")
    old_dm.setup("fit")

    batch_size = 2
    for k in range(10):
        batch = []
        single_batch = []
        old_batch = []
        for i in range(batch_size):
            batch.append(dm.train_set[k * batch_size + i])
            single_batch.append(single_dm.train_set[k * batch_size + i])
            old_batch.append(old_dm.train_set[k * batch_size + i])
        batch = new_transform.collate_fn(batch)
        single_batch = new_single_transform.collate_fn(single_batch)
        old_batch = old_transform.stack_spectrograms(old_batch)

        assert torch.allclose(
            batch["input"], old_batch["mix_frames"]
        ), "mix_frames not equal"
        assert torch.allclose(
            single_batch["input"], old_batch["mix_frames"]
        ), "mix_frames not equal"

        assert torch.allclose(
            batch["ground_truth"], old_batch["sources_frames"]
        ), "sources_frames not equal"

        assert torch.allclose(
            batch["num_frames"], torch.tensor(old_batch["num_frames"])
        ), "num_frames not equal"
        assert torch.allclose(
            single_batch["num_frames"], torch.tensor(old_batch["num_frames"])
        ), "num_frames not equal"

    dm.setup("test")
    single_dm.setup("test")
    old_dm.setup("test")

    for n, (batch, single_batch, old_batch) in enumerate(
        zip(dm.test_dataloader(), single_dm.test_dataloader(), old_dm.test_dataloader())
    ):
        assert torch.allclose(
            batch["input"], old_batch["mix_frames"]
        ), "mix_frames not equal"
        assert torch.allclose(
            single_batch["input"], old_batch["mix_frames"]
        ), "mix_frames not equal"

        assert torch.allclose(
            batch["ground_truth"], old_batch["sources_frames"]
        ), "sources_frames not equal"

        for phi_0, phi_1 in zip(batch["phases"], old_batch["phases"]):
            assert torch.allclose(phi_0, phi_1), "phases not equal"
        for phi_0, phi_1 in zip(single_batch["phases"], old_batch["phases"]):
            assert torch.allclose(phi_0, phi_1), "phases not equal"

        assert torch.allclose(
            batch["num_frames"], torch.tensor(old_batch["num_frames"])
        ), "num_frames not equal"
        assert torch.allclose(
            single_batch["num_frames"], torch.tensor(old_batch["num_frames"])
        ), "num_frames not equal"

        for x0, x1 in zip(batch["audios"], old_batch["audios"]):
            assert torch.allclose(x0, x1), "audios not equal"
        if n > 100:
            break


def test():
    with open("config/config0.yml", "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    config["network"]["input_dim"] = 513

    _test_ss_spec_transform(config)
