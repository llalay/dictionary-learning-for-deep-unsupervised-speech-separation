from SDM_VAE_src import models, dataprocessing, trainer
import yaml, pathlib, torch


def print_fill(text, filler_print: str = "*", filler_length: int = 100):
    print(f"{text:{filler_print}^{filler_length}}")


class AudioTrainer:
    def __init__(self, args: trainer.ArgNamespace) -> None:
        self.args = args
        self.load_config()
        logpath = pathlib.Path(self.config["training"]["root_dir"], "lightning_logs")
        self.best_path = logpath / "best_path"
        self.last_path = logpath / "last_path"
        self.load_transform()
        self.load_data()
        self.init_model()
        self.init_trainer(callbacks=self.model.callbacks)

    def load_config(self) -> None:
        with open(self.args.config_file, "r") as f:
            self.config = yaml.load(f, Loader=yaml.FullLoader)

    def load_transform(self) -> None:
        # Load the transform for the dataset
        transform_name = self.config["transform"]["name"]
        assert (
            transform_name in dataprocessing.supported_transforms.keys()
        ), f"""Transform {self.config['transform']['name']} not supported.
        Supported transforms: {dataprocessing.supported_transforms.keys()}"""
        self.transform = dataprocessing.supported_transforms[transform_name](
            self.config["transform"]
        )

    def load_data(self) -> None:
        # Load the dataset
        dataset_name = self.config["data"]["dataset"]
        assert (
            dataset_name in dataprocessing.supported_datasets.keys()
        ), f"""Dataset {self.config['data']['dataset']} not supported.
        Supported datasets: {dataprocessing.supported_datasets.keys()}"""

        self.datamodule = dataprocessing.supported_datasets[dataset_name](
            self.config["data"], self.transform
        )
        self.demo_batch = self.datamodule.demo_audio_batch(
            self.config["callbacks"]["log_audio"]["n_audios"]
        )
        self.config["network"]["input_dim"] = self.demo_batch["input_dim"]
        self.demo_batch["transform"] = self.transform

    def init_model(self) -> None:
        model_name = self.config["model"]["name"]
        assert (
            model_name in models.supported_models.keys()
        ), f"""Model {self.config['model']} not supported.
        Supported models: {models.supported_models.keys()}"""
        self.model = models.supported_models[model_name](
            self.config,
            self.demo_batch["batch"],
            self.transform,
        )

    def init_trainer(self, callbacks=[]) -> None:
        print("Initializing trainer...")
        self.trainer = trainer.LightningTrainer(
            self.config,
            self.demo_batch,
            callbacks,
            self.args,
        )

    def get_test_checkpoint(self) -> pathlib.Path:
        # Load the best checkpoint for testing
        try:
            with open(self.best_path, "r") as f:
                return pathlib.Path(f.read())
        except (FileNotFoundError, OSError) as e:
            print(e)
            return None

    def get_resume_checkpoint(self) -> pathlib.Path:
        # Load the last checkpoint to continue training
        try:
            with open(self.last_path, "r") as f:
                return pathlib.Path(f.read())
        except (FileNotFoundError, OSError) as e:
            print(e)
            return None

    def save_checkpoint(self) -> None:
        # Save the path to the best checkpoint
        with open(self.best_path, "w") as f:
            f.write(self.trainer.checkpoint_callback.best_model_path)

        # Save the path to the last checkpoint
        with open(self.last_path, "w") as f:
            f.write(self.trainer.checkpoint_callback.last_model_path)

        # Save the dictionaries
        logs_path = pathlib.Path(self.trainer.log_dir)
        torch.save(
            self.model.dictionary,
            logs_path / "dictionaries.pt",
        )

    def train(self) -> None:
        self.datamodule.setup("fit")

        if self.args.resume:
            ckpt = self.get_resume_checkpoint()
            self.trainer.fit(self.model, self.datamodule, ckpt_path=ckpt)
        else:
            self.trainer.fit(self.model, self.datamodule)

        if self.args.save:
            self.save_checkpoint()

        self.trainer.test(self.model, self.datamodule)

    def test(self) -> None:
        self.demo_batch = self.datamodule.demo_audio_batch(4)
        self.demo_batch["transform"] = self.transform

        ckpt = self.get_test_checkpoint()

        if ckpt is not None:
            self.trainer.test(self.model, self.datamodule, ckpt_path=ckpt)
        else:
            print("No checkpoint found. Provide a checkpoint to test the model.")
            exit()


def main():
    parser = trainer.TrainingParser()
    model_trainer = AudioTrainer(parser.args)

    if model_trainer.args.test_only:
        model_trainer.test()
        exit()
    model_trainer.train()


def main2():
    parser = trainer.TrainingParser()
    cli_args = parser.parse_args()

    with open(cli_args.config_file, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    transform = dataprocessing.SourceSeparationSpecTransform(config["transform"])
    datamodule = dataprocessing.TCDMixDatamodule(config, transform=transform)
    match config["model"]["name"]:
        case "sdm_supervised_dict_specific":
            model = models.sdm_supervised_dict_specific.SupervisedDictSpecific(
                config,
                speakers=datamodule.speakers,
                transform=transform,
            )
        case "sdm_pretrained":
            print("Loading pretrained model...")
            ckpt = "lightning_logs/Lib-SdmVae-Z64xD64-TimeLoss-0/checkpoints/last.ckpt"
            model = models.sdm_pretrained.SDMPretrained(
                config,
                datamodule.speakers,
                transform=transform,
                checkpoint=ckpt,
            )
    lightning_trainer = trainer.LightningTrainer(config=config, args=cli_args)

    lightning_trainer.fit(model, datamodule)


if __name__ == "__main__":
    try:
        main2()
    except KeyboardInterrupt:
        print_fill("Keyboard Interrupt")
        exit()
